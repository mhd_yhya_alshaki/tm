import 'package:flutter/cupertino.dart';

class ScreenContainer extends StatefulWidget {
  final Widget child;
  final EdgeInsets? padding;
  final bool useSafeArea;

  const ScreenContainer(
      {super.key, required this.child, this.padding, this.useSafeArea = false});

  @override
  State<ScreenContainer> createState() => _ScreenContainerState();
}

class _ScreenContainerState extends State<ScreenContainer> {
  @override
  Widget build(BuildContext context) {
    return widget.useSafeArea
        ? SafeArea(
            child: Container(
            padding: widget.padding,
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: widget.child,
          ))
        : Container(
            padding: widget.padding,
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: widget.child,
          );
  }
}
