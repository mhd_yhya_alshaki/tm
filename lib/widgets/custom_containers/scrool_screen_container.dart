import 'package:flutter/cupertino.dart';

class ScrollScreenContainer extends StatefulWidget {
  final List<Widget> children;
  final EdgeInsets? padding;
  final MainAxisAlignment? mainAxisAlignment;
  final DecorationImage? decorationImage;

  const ScrollScreenContainer(
      {super.key,
      required this.children,
      this.padding,
      this.mainAxisAlignment,
      this.decorationImage});

  @override
  State<ScrollScreenContainer> createState() => _ScrollScreenContainerState();
}

class _ScrollScreenContainerState extends State<ScrollScreenContainer> {
  @override
  Widget build(BuildContext context) {
    return Container(
        padding: widget.padding,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(image: widget.decorationImage),
        child: Center(
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment:
                  widget.mainAxisAlignment ?? MainAxisAlignment.center,
              children: widget.children,
            ),
          ),
        ));
  }
}
