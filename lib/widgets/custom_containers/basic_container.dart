import 'package:flutter/material.dart';

import '../../core/utils/resources/values_manager.dart';

class BasicContainer extends StatelessWidget {
  final Widget child;
  final double? width;
  final double? height;
  final Color? color;
  final BoxBorder? boxBorder;
  final EdgeInsetsGeometry? padding;
  final EdgeInsetsGeometry? margin;
  final List<BoxShadow>? boxShadow;
  final BoxShape shape;

  const BasicContainer(
      {super.key,
      required this.child,
      this.width,
      this.height,
      this.padding,
      this.color,
      this.boxBorder,
      this.boxShadow,
      this.margin,
      this.shape = BoxShape.rectangle});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: color ?? Theme.of(context).colorScheme.primaryContainer,
          borderRadius: shape == BoxShape.circle
              ? null
              : BorderRadius.circular(AppSizeR.s10),
          boxShadow: boxShadow,
          shape: shape,
          border: boxBorder),
      clipBehavior: Clip.antiAlias,
      padding: padding,
      margin: margin,
      width: width,
      height: height,
      child: child,
    );
  }
}
