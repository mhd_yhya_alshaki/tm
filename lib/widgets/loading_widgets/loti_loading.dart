import 'package:flutter/cupertino.dart';
import 'package:lottie/lottie.dart';
import 'package:tm/generated/assets.dart';
import '../../core/utils/helpers/helper.dart';

class LotiLoading extends StatelessWidget {
  const LotiLoading({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: Helper.instance.paddingHelper.bigSymmetricPadding,
        child: Lottie.asset(Assets.loadingLoading),
      ),
    );
  }
}
