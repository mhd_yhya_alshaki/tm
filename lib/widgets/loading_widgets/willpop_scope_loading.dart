import 'package:flutter/material.dart';
import 'package:tm/widgets/loading_widgets/loti_loading.dart';

import '../../core/utils/resources/color_manager.dart';

class LoadingDialog extends StatefulWidget {
  const LoadingDialog({super.key});

  @override
  LoadingDialogState createState() => LoadingDialogState();

  static void show(BuildContext context) {
    showDialog(
      context: context,
      barrierDismissible: false,
      barrierColor: ColorManager.lightBlack.withOpacity(.4),
      builder: (BuildContext context) {
        return const LoadingDialog();
      },
    );
  }

  static void hide(BuildContext context) {
    if (Navigator.of(
      context,
    ).canPop()) {
      Navigator.of(
        context,
      ).pop(context);
    }
  }
}

class LoadingDialogState extends State<LoadingDialog> {
  @override
  Widget build(BuildContext context) {
    return const PopScope(canPop: false, child: LotiLoading());
  }
}
