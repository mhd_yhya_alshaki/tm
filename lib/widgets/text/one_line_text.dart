import 'package:flutter/material.dart';

class OneLineText extends StatelessWidget {
  final String? value;
  final TextStyle? style;

  const OneLineText(this.value, {super.key, this.style});

  @override
  Widget build(BuildContext context) {
    return Text(value ?? '', maxLines: 1, overflow: TextOverflow.ellipsis);
  }
}
