import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:tm/generated/locale_keys.g.dart';
import 'package:vibration/vibration.dart';

import 'custom_buttons/delete_btn.dart';
import 'custom_buttons/edit_btn.dart';

class SlidableAction extends StatelessWidget {
  final Widget child;
  final Function()? onEdit;
  final Function()? onDelete;

  const SlidableAction({
    super.key,
    required this.child,
    this.onEdit,
    this.onDelete,
  });

  @override
  Widget build(BuildContext context) {
    bool canVibrate = true;
    return Dismissible(
      key: UniqueKey(),
      background: Container(
        alignment: Alignment.centerLeft,
        child: const Padding(
          padding: EdgeInsets.only(left: 16),
          child: EditButton(),
        ),
      ),
      secondaryBackground: Container(
        alignment: Alignment.centerRight,
        child: const Padding(
          padding: EdgeInsets.only(right: 16),
          child: DeleteButton(),
        ),
      ),
      behavior: HitTestBehavior.deferToChild,
      crossAxisEndOffset: 0.3,
      onUpdate: (details) async {
        if (details.reached &&
            (await Vibration.hasVibrator() ?? false) &&
            canVibrate) {
          canVibrate = !canVibrate;
          Vibration.vibrate(duration: 200);
        } else if (!details.reached) {
          canVibrate = true;
        }
      },
      onResize: () {},
      confirmDismiss: (direction) async {
        if (direction == DismissDirection.startToEnd) {
          if (onEdit != null) {
            onEdit!();
          }
        } else if (await showConfirmationDialog(
                context, LocaleKeys.action_delete.tr(), onDelete) ??
            false) {}
        return null;
        // if (direction == DismissDirection.startToEnd) {
        //   return true;
        // } else {
        //   return await showConfirmationDialog(
        //       context, LocaleKeys.action_delete.tr(), onDelete);
        // }
      },
      child: child,
      onDismissed: (direction) async {
        // if (await Vibration.hasVibrator() ?? false) {
        //   Vibration.vibrate();
        // }
        // if (direction == DismissDirection.startToEnd) {
        //   if (onEdit != null) {
        //     onEdit!();
        //   }
        // } else {
        //   if (onDelete != null) {
        //     onDelete!();
        //   }
        // }
      },
    );
  }

  Future<bool?> showConfirmationDialog(BuildContext context, String actionName,
      Function()? actionCallback) async {
    return await showDialog<bool>(
      context: context,
      builder: (context) => AlertDialog(
        title: const Text('Confirmation'),
        content: Text('Are you sure you want to $actionName this item?'),
        actions: [
          TextButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: const Text('Cancel'),
          ),
          TextButton(
            onPressed: () {
              Navigator.of(context).pop(true);
              actionCallback?.call();
            },
            child: const Text('Yes'),
          ),
        ],
      ),
    );
  }
}
