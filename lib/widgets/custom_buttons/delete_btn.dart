import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../core/utils/resources/values_manager.dart';

class DeleteButton extends StatelessWidget {
  final Function()? onRemovePressed;

  const DeleteButton({super.key, this.onRemovePressed});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.transparent,
      child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            padding: EdgeInsets.symmetric(
                horizontal: AppSizeH.s1, vertical: AppSizeH.s1),
            minimumSize: Size(AppSizeH.s35, AppSizeH.s35),
            backgroundColor: Theme.of(context).cardColor,
          ),
          onPressed: onRemovePressed,
          child: Icon(
            Icons.delete_rounded,
            size: 25.sp,
            color: Theme.of(context).colorScheme.error,
          )),
    );
  }
}
