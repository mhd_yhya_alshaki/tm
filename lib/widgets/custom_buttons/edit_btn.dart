import 'package:flutter/material.dart';

import '../../core/utils/resources/values_manager.dart';

class EditButton extends StatelessWidget {
  final Function()? onPressed;

  const EditButton({super.key, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: AppSizeH.s50,
      child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            padding: EdgeInsets.symmetric(
                horizontal: AppSizeH.s15, vertical: AppSizeH.s15),
            minimumSize: Size(AppSizeW.s15, AppSizeH.s15),
            backgroundColor: Theme.of(context).cardColor,
          ),
          onPressed: onPressed,
          child: Icon(
            Icons.edit,
            color: Theme.of(context).colorScheme.error,
          )),
    );
  }
}
