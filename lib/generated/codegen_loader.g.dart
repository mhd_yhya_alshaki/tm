// DO NOT EDIT. This is code generated via package:easy_localization/generate.dart

// ignore_for_file: prefer_single_quotes, avoid_renaming_method_parameters

import 'dart:ui';

import 'package:easy_localization/easy_localization.dart' show AssetLoader;

class CodegenLoader extends AssetLoader{
  const CodegenLoader();

  @override
  Future<Map<String, dynamic>?> load(String path, Locale locale) {
    return Future.value(mapLocales[locale.toString()]);
  }

  static const Map<String,dynamic> en = {
  "appName": "App Name",
  "dioError": {
    "noInternetError": "No Internet"
  },
  "action": {
    "save": "Save",
    "cancel": "Cancel",
    "yes": "Yes",
    "delete": "Delete",
    "add": "Add",
    "select": "Select",
    "clear": "Clear"
  },
  "auth": {
    "userName": "User Name",
    "password": "Password",
    "login": "Login",
    "logOut": "LogOut"
  },
  "updateNow": "Update Now",
  "dialogMessage": {
    "deActivateTheItemMessage": "Are You Sure You Want DeActivate The item?",
    "ActivateTheItemMessage": "Are You Sure You Want Activate The item?",
    "deleteMessage": "Are You Sure You Want Delete The item?",
    "logOutMessage": "Are You Sure You Want To LogOut?",
    "discardSave": "The information will not be saved",
    "updateMessage": "A new version of the app is available. Please update to continue."
  },
  "dialogTitle": {
    "delete": "Delete"
  },
  "success": "Success",
  "from": "From: {}",
  "to": "To: {}",
  "fromDate": "From Date",
  "toDate": "To Date"
};
static const Map<String, Map<String,dynamic>> mapLocales = {"en": en};
}
