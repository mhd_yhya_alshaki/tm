// DO NOT EDIT. This is code generated via package:easy_localization/generate.dart

abstract class  LocaleKeys {
  static const appName = 'appName';
  static const dioError_noInternetError = 'dioError.noInternetError';
  static const dioError = 'dioError';
  static const action_save = 'action.save';
  static const action_cancel = 'action.cancel';
  static const action_yes = 'action.yes';
  static const action_delete = 'action.delete';
  static const action_add = 'action.add';
  static const action_select = 'action.select';
  static const action_clear = 'action.clear';
  static const action = 'action';
  static const auth_userName = 'auth.userName';
  static const auth_password = 'auth.password';
  static const auth_login = 'auth.login';
  static const auth_logOut = 'auth.logOut';
  static const auth = 'auth';
  static const updateNow = 'updateNow';
  static const dialogMessage_deActivateTheItemMessage = 'dialogMessage.deActivateTheItemMessage';
  static const dialogMessage_ActivateTheItemMessage = 'dialogMessage.ActivateTheItemMessage';
  static const dialogMessage_deleteMessage = 'dialogMessage.deleteMessage';
  static const dialogMessage_logOutMessage = 'dialogMessage.logOutMessage';
  static const dialogMessage_discardSave = 'dialogMessage.discardSave';
  static const dialogMessage_updateMessage = 'dialogMessage.updateMessage';
  static const dialogMessage = 'dialogMessage';
  static const dialogTitle_delete = 'dialogTitle.delete';
  static const dialogTitle = 'dialogTitle';
  static const success = 'success';
  static const from = 'from';
  static const to = 'to';
  static const fromDate = 'fromDate';
  static const toDate = 'toDate';

}
