import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tm/app/bloc_observer.dart';
import 'package:tm/core/constants/constant.dart';
import 'package:tm/core/utils/language/language_manager.dart';

import 'app/app.dart';
import 'app/dependency_injection.dart';
import 'generated/codegen_loader.g.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);
  await initAppModule();
  Bloc.observer = MyBlocObserver();
  runApp(EasyLocalization(
      supportedLocales: AppLanguage.supportedLocales,
      path: Constants.localizationAssetsPath,
      assetLoader: const CodegenLoader(),
      fallbackLocale: AppLanguage.fallbackLocale,
      child: const MyApp()));
}
