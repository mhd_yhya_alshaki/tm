import 'package:bloc/bloc.dart';
// import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../core/utils/helpers/helper_imports.dart';
import '../domain/usecases/auth.dart';
import '../entities/auth/auth_parameters.dart';

part 'auth_bloc.freezed.dart';
part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final LoginUseCase _loginUseCase = instance<LoginUseCase>();
  final appPreferences = instance<AppPreferences>();

  AuthBloc() : super(const AuthState.initial()) {
    on<AuthEvent>((event, emit) async {
      await event.map(
        login: (value) async => await _handleLogin(emit, value),
        logout: (value) {},
      );
    });
  }

  Future<void> _handleLogin(Emitter<AuthState> emit, _LogInEvent event) async {
    try {
      emit(const AuthState.loading());
      await Future.sync(() async => (await _loginUseCase.execute(AuthParameters(
            userName: event.parameters.userName,
            password: event.parameters.password,
            // fcmToken: await FirebaseMessaging.instance.getToken()
          )))
              .when((success) async {
            // await instance.reset();
            await appPreferences.userPreferences.setUserToken(success.token);
            await appPreferences.userPreferences.saveUserData(success);
            await instance.reset();
            await initAppModule();
            emit(const AuthState.logInSuccess());
          }, (error) => emit(AuthState.logInError(error.message))));
    } catch (e) {
      emit(AuthState.logInError(e.toString()));
    }
  }
}
