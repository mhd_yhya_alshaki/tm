part of 'auth_bloc.dart';

@freezed
class AuthEvent with _$AuthEvent {
  const factory AuthEvent.login({required AuthParameters parameters}) =
      _LogInEvent;
  const factory AuthEvent.logout() = _LogOutEvent;
}
