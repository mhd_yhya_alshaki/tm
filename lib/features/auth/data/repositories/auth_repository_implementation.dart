import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:multiple_result/multiple_result.dart';

import '../../../../core/models/failure_model/failure.dart';
import '../../../../core/network/network_info.dart';
import '../../../../generated/locale_keys.g.dart';
import '../../domain/repositories/auth_repository.dart';
import '../../entities/auth/auth_parameters.dart';
import '../../entities/user/user.dart';
import '../datasources/auth_datasource.dart';

class AuthRepositoryImplementation implements AuthRepository {
  final AuthDatasource datasource;

  final NetworkInfo _networkInfo;

  AuthRepositoryImplementation(this.datasource, this._networkInfo);

  @override
  Future<Result<UserModel, FailureModel>> login(
      AuthParameters parameters) async {
    if (await _networkInfo.isConnected) {
      try {
        final response = await datasource.login(
          password: parameters.password,
          username: parameters.userName,
        );
        if (response.response.statusCode == 200 ||
            response.response.statusCode == 201) {
          return Success(response.data);
        } else {
          return Error(FailureModel.fromJson(response.response.data));
        }
      } on DioException catch (e) {
        if (e.type == DioExceptionType.cancel) {
          return Error(
              FailureModel(message: DioExceptionType.cancel.toString()));
        }
        return Error(FailureModel.fromJson(e.response?.data ?? defaultError));
      }
    } else {
      return Error(
          FailureModel(message: LocaleKeys.dioError_noInternetError.tr()));
    }
  }
}
