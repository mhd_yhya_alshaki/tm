import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';

import '../../../../core/constants/constant.dart';
import '../../entities/user/user.dart';
import '../auth_endpoints/auth_endpoints.dart';

part 'auth_datasource.g.dart';

@RestApi(baseUrl: Constants.apiUrl)
abstract class AuthDatasource {
  factory AuthDatasource(Dio dio, {String baseUrl}) = _AuthDatasource;

  @POST(AuthEndPoints.login)
  Future<HttpResponse<UserModel>> login(
      {@Field() required String username, @Field() required String password});
}
