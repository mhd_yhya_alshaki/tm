import 'package:multiple_result/multiple_result.dart';

import '../../../../core/models/failure_model/failure.dart';
import '../../entities/auth/auth_parameters.dart';
import '../../entities/user/user.dart';

abstract class AuthRepository {
  Future<Result<UserModel, FailureModel>> login(AuthParameters parameters);
}
