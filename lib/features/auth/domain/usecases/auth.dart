import 'package:multiple_result/multiple_result.dart';

import '../../../../core/models/basic_use_cases/basic_use_cases.dart';
import '../../../../core/models/failure_model/failure.dart';
import '../../entities/auth/auth_parameters.dart';
import '../../entities/user/user.dart';
import '../repositories/auth_repository.dart';

class LoginUseCase implements BaseUseCase<AuthParameters, UserModel> {
  final AuthRepository repository;

  LoginUseCase(this.repository);

  @override
  Future<Result<UserModel, FailureModel>> execute(
      AuthParameters parameters) async {
    return await repository.login(parameters);
  }
}
