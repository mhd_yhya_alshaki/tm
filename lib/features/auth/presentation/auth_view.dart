import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:tm/widgets/text_fields/custom_text_field_up_beside.dart';

// import 'package:tm/widgets/text_fields/custom_text_field_up_beside.dart';

import '../../../../core/router/routes_manager.dart';
import '../../../../core/utils/resources/validator.dart';
import '../../../../features/auth/bloc/auth_bloc.dart';
import '../../../../features/auth/entities/auth/auth_parameters.dart';
import '../../../../generated/assets.dart';
import '../../../../generated/locale_keys.g.dart';
import '../../../../widgets/custom_buttons/custom_elevated_button.dart';
import '../../../../widgets/custom_containers/basic_container.dart';
import '../../../../widgets/custom_containers/screen_container.dart';
import '../../../core/utils/helpers/helper.dart';

class AuthPage extends StatelessWidget {
  const AuthPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<AuthBloc>(
      create: (context) => AuthBloc(),
      child: const AuthView(),
    );
  }
}

class AuthView extends StatefulWidget {
  const AuthView({super.key});

  @override
  State<AuthView> createState() => _AuthViewState();
}

class _AuthViewState extends State<AuthView> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  late final AuthBloc authBloc = context.read<AuthBloc>();
  final AuthParameters _authParameters =
      AuthParameters(userName: '', password: '');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ScreenContainer(
          useSafeArea: true,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Flexible(
                  flex: 2,
                  child: Padding(
                    padding:
                        Helper.instance.paddingHelper.bigSymmetricPadding * 3,
                    child: Image.asset(
                      Assets.logosLogo,
                    ),
                  )),
              SizedBox(
                height: 4.h,
              ),
              BasicContainer(
                  padding: Helper.instance.paddingHelper.bigSymmetricPadding,
                  margin: Helper.instance.paddingHelper.smallVertical,
                  width: 300.w,
                  child: Form(
                    key: _formKey,
                    child: Column(
                      children: [
                        CustomTextFieldUpBeside(
                          label: LocaleKeys.auth_userName.tr(),
                          prefixIcon: const Icon(Icons.person),
                          onChanged: (value) =>
                              _authParameters.userName = value,
                          validator: Validator.validateRequiredFiled,
                        ),
                        CustomTextFieldUpBeside(
                          label: LocaleKeys.auth_password.tr(),
                          prefixIcon: const Icon(Icons.lock),
                          onChanged: (value) =>
                              _authParameters.password = value,
                          validator: Validator.validateRequiredFiled,
                          isPassword: true,
                        ),
                        SizedBox(
                          height: Helper.instance.paddingHelper
                                  .bigSymmetricPadding.horizontal /
                              2,
                        ),
                        BlocConsumer<AuthBloc, AuthState>(
                          listener: (context, state) {
                            state.mapOrNull(
                              logInError: (value) => Helper
                                  .instance.messageHelper
                                  .showErrorMessage(
                                      message: value.error, context: context),
                              logInSuccess: (value) {
                                appRouter.goNamed(RoutesNames.homeRoute);
                              },
                            );
                          },
                          builder: (context, state) {
                            return CustomElevatedButton(
                                isLoading: state.maybeMap(
                                  orElse: () => false,
                                  loading: (value) => true,
                                ),
                                onPressed: () {
                                  if (_formKey.currentState?.validate() ??
                                      false) {
                                    authBloc.add(AuthEvent.login(
                                        parameters: _authParameters));
                                  }
                                },
                                child: Text(LocaleKeys.auth_login.tr()));
                          },
                        )
                      ],
                    ),
                  )),
              const Spacer(
                flex: 1,
              )
            ],
          )),
    );
  }
}
