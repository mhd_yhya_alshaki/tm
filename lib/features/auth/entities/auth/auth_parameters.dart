class AuthParameters {
  String userName;
  String password;
  String? fcmToken;

  AuthParameters({this.userName = '', this.password = '', this.fcmToken});
}
