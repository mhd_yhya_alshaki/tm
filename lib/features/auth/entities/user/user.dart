import 'package:freezed_annotation/freezed_annotation.dart';

part 'user.freezed.dart';
part 'user.g.dart';

@freezed
class UserModel with _$UserModel {
  const factory UserModel(
      {@Default(-1) int id,
      @Default('') String username,
      @Default('') String email,
      @Default('') String firstName,
      @Default('') String lastName,
      @Default('') String gender,
      @Default('') String image,
      @Default('') String token,
      @Default('') String refreshToken}) = _UserModel;

  factory UserModel.fromJson(Map<String, dynamic> json) =>
      _$UserModelFromJson(json);
}
