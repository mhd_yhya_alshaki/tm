import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:tm/core/utils/helpers/helper.dart';
import 'package:tm/core/utils/helpers/helper_imports.dart';
import 'package:tm/core/utils/resources/resources.dart';
import 'package:tm/generated/assets.dart';
import 'package:tm/generated/locale_keys.g.dart';
import 'package:tm/widgets/custom_containers/basic_container.dart';
import 'package:tm/widgets/custom_containers/screen_container.dart';
import 'package:tm/widgets/dialog/confirm_dialog.dart';

class HomeDrawerWidget extends StatelessWidget {
  // final AdvancedDrawerController controller;

  const HomeDrawerWidget({
    super.key,
  });

  pushToScreen(String screenName) {
    // controller.hideDrawer();
    Future.delayed(
      DurationConstant.d300,
      () {
        appRouter.goNamed(screenName);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      backgroundColor: Theme.of(context).cardColor,
      child: ScreenContainer(
          child: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Center(
              child: Container(
                padding: Helper.instance.paddingHelper.bigSymmetricPadding * 3,
                child: Column(
                  children: [
                    BasicContainer(
                        shape: BoxShape.circle,
                        height: 100.h,
                        width: 100.h,
                        padding:
                            Helper.instance.paddingHelper.bigSymmetricPadding,
                        child: Image.asset(Assets.logosLogo)),
                    SizedBox(
                      height: 10.h,
                    ),
                    Text(
                      instance<AppPreferences>()
                          .userPreferences
                          .userData
                          .firstName,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 10.h,
            ),
            Expanded(
              child: SingleChildScrollView(
                padding: Helper.instance.paddingHelper.mediumHorizontal,
                child: const Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    // TextButton.icon(
                    //     icon: const Icon(Icons.person),
                    //     onPressed: () {
                    //       pushToScreen(RoutesNames.profilePageRoute);
                    //     },
                    //     label: Text(LocaleKeys.profile.tr())),
                    // SizedBox(
                    //   height: 10.h,
                    // ),
                    // TextButton.icon(
                    //     icon: const Icon(
                    //         CupertinoIcons.antenna_radiowaves_left_right),
                    //     onPressed: () {
                    //       pushToScreen(RoutesNames.advertisementPageRoute);
                    //     },
                    //     label: Text(LocaleKeys.advertisement.tr())),
                    // SizedBox(
                    //   height: 10.h,
                    // ),
                    // TextButton.icon(
                    //     icon: const Icon(CupertinoIcons.star_circle),
                    //     onPressed: () =>
                    //         pushToScreen(RoutesNames.ratePageRoute),
                    //     label: Text(LocaleKeys.rating.tr())),
                    // SizedBox(
                    //   height: 10.h,
                    // ),
                    // TextButton.icon(
                    //     icon: const Icon(Icons.language_rounded),
                    //     onPressed: () {
                    //       controller.hideDrawer();
                    //       Future.delayed(
                    //         DurationConstant.d300,
                    //         () {
                    //           Helper.instance.routerHelper.openLink(
                    //               Constants.webFrontUrl +
                    //                   instance<AppPreferences>()
                    //                       .userPreferences
                    //                       .userData()
                    //                       .name);
                    //         },
                    //       );
                    //     },
                    //     label: Text(LocaleKeys.theWebsite.tr())),
                    // SizedBox(
                    //   height: 10.h,
                    // ),
                    // TextButton.icon(
                    //     icon: const Icon(Icons.settings),
                    //     onPressed: () =>
                    //         pushToScreen(RoutesNames.settingsPageRoute),
                    //     label: Text(LocaleKeys.settings.tr())),
                  ],
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal: 20.w, vertical: 10.h),
                  child: IconButton(
                      style: Theme.of(context).errorTextButtonTheme,
                      onPressed: () {
                        showDialog(
                          context: context,
                          builder: (context) => ConfirmDialog(
                              isDanger: true,
                              message:
                                  LocaleKeys.dialogMessage_logOutMessage.tr(),
                              title: LocaleKeys.auth_logOut.tr()),
                        ).then((value) {
                          if (value == true) {
                            Helper.instance.routerHelper.logoutWithoutApi();
                          }
                        });
                      },
                      icon: Icon(
                        Icons.logout_rounded,
                        size: 40.sp,
                      )),
                ),
              ],
            ),
          ],
        ),
      )),
    );
  }
}
