import 'package:flutter/material.dart';

class FloatHomeBtnWidget extends StatelessWidget {
  const FloatHomeBtnWidget({super.key});

  @override
  Widget build(BuildContext context) {
    // final key = GlobalKey<ExpandableFabState>();
    // return ExpandableFab(
    //   key: key,
    //   openButtonBuilder: RotateFloatingActionButtonBuilder(
    //     child: const Icon(Icons.add),
    //     fabSize: ExpandableFabSize.regular,
    //     shape: const CircleBorder(),
    //   ),
    //   type: ExpandableFabType.up,
    //   overlayStyle: ExpandableFabOverlayStyle(blur: 5),
    //   closeButtonBuilder: FloatingActionButtonBuilder(
    //     size: 56,
    //     builder: (BuildContext context, void Function()? onPressed,
    //         Animation<double> progress) {
    //       return IconButton(
    //         onPressed: onPressed,
    //         icon: const Icon(
    //           Icons.close,
    //           size: 40,
    //         ),
    //       );
    //     },
    //   ),
    //   children: [
    //     FloatingActionButton.extended(
    //       label: Text(LocaleKeys.item.tr()),
    //       heroTag: null,
    //       onPressed: () {
    //         key.currentState?.toggle();
    //         Future.delayed(
    //           Durations.medium1,
    //           () {
    //             appRouter.pushNamed(
    //               RoutesNames.crudProductPagePageRoute,
    //             );
    //           },
    //         );
    //       },
    //     ),
    //     FloatingActionButton.extended(
    //         label: Text(LocaleKeys.subCategory.tr()),
    //         heroTag: null,
    //         onPressed: () {
    //           key.currentState?.toggle();
    //           Future.delayed(
    //             Durations.medium1,
    //             () {
    //               appRouter.pushNamed(
    //                 RoutesNames.crudSubCategoryPageRoute,
    //               );
    //             },
    //           );
    //         }),
    //     FloatingActionButton.extended(
    //       label: Text(LocaleKeys.category.tr()),
    //       heroTag: null,
    //       onPressed: () {
    //         key.currentState?.toggle();
    //         Future.delayed(
    //           Durations.medium1,
    //           () {
    //             appRouter.goNamed(RoutesNames.crudCategoryPageRoute);
    //           },
    //         );
    //       },
    //     ),
    //     // IconButton(
    //     //   onPressed: () {
    //     //     final state = _key.currentState;
    //     //     if (state != null) {
    //     //       debugPrint('isOpen:${state.isOpen}');
    //     //       state.toggle();
    //     //     }
    //     // },
    //     // icon: const Icon(
    //     //   Icons.share,
    //     //   size: 30,
    //     // ),
    //     // ),
    //   ],
    // );
    return const SizedBox();
  }
}
