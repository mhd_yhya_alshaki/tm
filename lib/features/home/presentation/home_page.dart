import 'package:easy_localization/easy_localization.dart';
import 'package:tm/core/utils/helpers/helper.dart';
import 'package:tm/core/utils/helpers/helper_imports.dart';
import 'package:tm/features/home/widgets/home_drawer.dart';
import 'package:tm/features/task/domain/repositories/task_repository.dart';
import 'package:tm/features/task/presentation/task_page.dart';
import 'package:tm/generated/locale_keys.g.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  // final _advancedDrawerController = AdvancedDrawerController();

  @override
  void initState() {
    super.initState();
    Future.delayed(
      Durations.extralong1,
      () {
        Helper.instance.messageHelper.showInfoMessage(
            context: context,
            message: 'Swipe Item left or Right to Delete or Edit');
      },
    );
    // Example: Sync tasks when the app starts
  }

  Future<void> _syncTasks() async {
    await instance<TaskRepository>().syncTasks();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const HomeDrawerWidget(),
      appBar: AppBar(
        title: Text(LocaleKeys.appName.tr()),
        leading: const DrawerButton(),
        actions: [
          FutureBuilder(
            future: _syncTasks(),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return IconButton(
                  onPressed: null,
                  icon: const CircularProgressIndicator(),
                );
              } else if (snapshot.connectionState == ConnectionState.done) {
                return IconButton(
                  onPressed: () async {
                    setState(() {
                      _syncTasks();
                    });
                  },
                  icon: const Icon(Icons.sync),
                );
              } else {
                return IconButton(
                  onPressed: null,
                  icon: const Icon(Icons.error),
                );
              }
            },
          ),
          IconButton(onPressed: () {}, icon: const Icon(Icons.notifications)),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          appRouter.goNamed(RoutesNames.crudTaskRoute);
        },
        child: const Icon(Icons.add),
      ),
      body: const TaskPage(),
    );
  }
}
