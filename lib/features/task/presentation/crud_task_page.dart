import 'dart:io';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:tm/core/utils/extensions/extensions.dart';
import 'package:tm/core/utils/helpers/helper_imports.dart';
import 'package:tm/core/utils/resources/validator.dart';
import 'package:tm/features/task/bloc/task_bloc.dart';
import 'package:tm/features/task/domain/usecases/task.dart';
import 'package:tm/features/task/entities/task_model/task.dart';
import 'package:tm/generated/locale_keys.g.dart';
import 'package:tm/widgets/custom_containers/basic_container.dart';
import 'package:tm/widgets/custom_containers/screen_container.dart';
import 'package:tm/widgets/loading_widgets/loti_loading.dart';
import 'package:tm/widgets/loading_widgets/small_circular_progress_indicator.dart';
import 'package:tm/widgets/text_fields/custom_text_field_up_beside.dart';

import '../../../app/dependency_injection.dart';
import '../../../core/utils/helpers/helper.dart';
import '../../../widgets/custom_buttons/custom_elevated_button.dart';
import '../../../widgets/custom_buttons/retry_btn.dart';

class CrudTaskPage extends StatelessWidget {
  final String? id;

  const CrudTaskPage({super.key, this.id});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<TaskCubit>.value(
      value: instance<TaskCubit>(),
      child: CrudTaskView(
        id: id,
      ),
    );
  }
}

class CrudTaskView extends StatefulWidget {
  final String? id;

  const CrudTaskView({super.key, this.id});

  @override
  State<CrudTaskView> createState() => _CrudTaskViewState();
}

class _CrudTaskViewState extends State<CrudTaskView> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool readOnly = false;
  late bool isEdit = widget.id != null;
  Task task = Task();

  @override
  void initState() {
    if (widget.id != null) {
      instance<TaskCubit>().showTask(widget.id ?? '');
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          BlocConsumer<TaskCubit, TaskState>(
            bloc: instance<TaskCubit>(),
            listener: (context, state) {
              if (state.status.hasError) {
                Helper.instance.messageHelper
                    .showErrorMessage(message: state.errorMessage ?? '');
              } else if (state.status == CrudBlocStatus.showSuccess) {
                task = state.currentShowTask ?? task;
              }
            },
            builder: (context, state) {
              if (state.status == CrudBlocStatus.loadingShow) {
                return const SizedBox();
              }
              if (state.status == CrudBlocStatus.showError) {
                return const SizedBox();
              }
              return Row(
                children: [
                  CustomElevatedButton(
                      isLoading: state.status.isLoading,
                      showShadow: false,
                      onPressed: () {
                        if ((_formKey.currentState?.validate() ?? false)) {
                          if (!isEdit) {
                            instance<TaskCubit>().createTask(task.copyWith(
                                userId: instance<AppPreferences>()
                                    .userPreferences
                                    .userData
                                    .id));
                          } else if (isEdit) {
                            instance<TaskCubit>().updateTask(task.copyWith(
                                userId: instance<AppPreferences>()
                                    .userPreferences
                                    .userData
                                    .id));
                          }
                        }
                      },
                      child: const Icon(
                        Icons.save,
                        color: Colors.white,
                      )),
                  SizedBox(
                    width: 20.w,
                  )
                ],
              );
            },
          )
        ],
      ),
      body: BlocConsumer<TaskCubit, TaskState>(
        bloc: instance<TaskCubit>(),
        listener: (context, state) {
          if (state.status.hasError) {
            Helper.instance.messageHelper.showErrorMessage(
                context: context, message: state.errorMessage ?? '');
          } else if (state.status.isSuccess) {
            Helper.instance.messageHelper.showSuccessMessage(
                context: context, message: LocaleKeys.success.tr());
          }
        },
        builder: (context, state) {
          return BlocConsumer<TaskCubit, TaskState>(
            bloc: instance<TaskCubit>(),
            listener: (context, state) {
              if (state.status.hasError) {
                Helper.instance.messageHelper
                    .showErrorMessage(message: state.errorMessage ?? '');
              } else if (state.status == CrudBlocStatus.showSuccess) {
                task = state.currentShowTask ?? task;
              }
            },
            builder: (context, state) {
              if (state.status == CrudBlocStatus.loadingShow) {
                return const Center(
                  child: LotiLoading(),
                );
              }
              if (state.status == CrudBlocStatus.showError) {
                return RetryBtn(
                  onTap: () {
                    instance<TaskCubit>().showTask(widget.id ?? '');
                  },
                );
              }
              return ScreenContainer(
                  child: SingleChildScrollView(
                padding: EdgeInsets.only(bottom: 20.h),
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      Padding(
                        padding: Helper.instance.paddingHelper.bigHorizontal,
                        child: Column(
                          children: [
                            SizedBox(
                              height: 50.h,
                            ),
                            CustomTextFieldUpBeside(
                              label: 'Task'.trans,
                              isTextArea: true,
                              value: task.todo,
                              maxLines: 10,
                              validator: Validator.validateRequiredFiled,
                              onChanged: (newValue) {
                                setState(() {
                                  task = task.copyWith(todo: newValue);
                                });
                              },
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ));
            },
          );
        },
      ),
    );
  }
}
