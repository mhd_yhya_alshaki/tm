import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_animate/flutter_animate.dart%20';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:tm/app/dependency_injection.dart';
import 'package:tm/core/utils/helpers/helper.dart';
import 'package:tm/features/task/bloc/task_bloc.dart';
import 'package:tm/features/task/widgets/task_card.dart';
import 'package:tm/generated/locale_keys.g.dart';
import 'package:tm/widgets/custom_buttons/retry_btn.dart';
import 'package:tm/widgets/custom_containers/screen_container.dart';
import 'package:tm/widgets/loading_widgets/small_circular_progress_indicator.dart';
import 'package:tm/widgets/loading_widgets/willpop_scope_loading.dart';

class TaskPage extends StatelessWidget {
  const TaskPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => instance<TaskCubit>()..fetchTasks(),
      child: const TaskView(),
    );
  }
}

class TaskView extends StatelessWidget {
  const TaskView({super.key});

  @override
  Widget build(BuildContext context) {
    final RefreshController _refreshController =
        RefreshController(initialRefresh: false);
    return BlocConsumer<TaskCubit, TaskState>(
      bloc: instance<TaskCubit>(),
      listener: (BuildContext context, TaskState state) {
        if (state.status.hasError) {
          LoadingDialog.hide(context);
          Helper.instance.messageHelper.showErrorMessage(
              context: context, message: state.errorMessage ?? '');
        } else if (state.status.isSuccess) {
          LoadingDialog.hide(context);
          Helper.instance.messageHelper.showSuccessMessage(
              context: context, message: LocaleKeys.success.tr());
        }
        _refreshController.loadComplete();
        _refreshController.refreshCompleted();
      },
      builder: (context, state) {
        return ScreenContainer(
            child: SmartRefresher(
                enablePullDown: true,
                enablePullUp: true,
                controller: _refreshController,
                onLoading: () => instance<TaskCubit>().loadMoreTasks(),
                onRefresh: () => instance<TaskCubit>().fetchTasks(),
                child: (state.status == CrudBlocStatus.loading)
                    ? const Center(child: SmallCircularIndicator())
                    : (state.status == CrudBlocStatus.error)
                        ? Center(
                            child: RetryBtn(
                              onTap: () => instance<TaskCubit>().fetchTasks(),
                            ),
                          )
                        : ListView.separated(
                            primary: false,
                            padding: Helper
                                .instance.paddingHelper.bigSymmetricPadding,
                            itemBuilder: (context, index) =>
                                TaskCard(task: state.tasks.elementAt(index)),
                            separatorBuilder: (context, index) => SizedBox(
                                  height: 20.h,
                                ),
                            itemCount: state.tasks.length)));
      },
    );
  }
}
