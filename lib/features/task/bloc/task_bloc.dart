import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:tm/core/models/basic_input/pagination_input/pagination_input.dart';
import 'package:tm/features/task/domain/usecases/task.dart';
import 'package:tm/features/task/entities/task_model/task.dart';

import '../../../app/dependency_injection.dart';
import '../../../core/utils/local_storage/app_preferences.dart';

part 'task_bloc.freezed.dart';
part 'task_state.dart';

class TaskCubit extends Cubit<TaskState> {
  final FetchTasksUseCase _fetchTasksUseCase;
  final UpdateTaskUseCase _updateTaskUseCase;
  final CreateTaskUseCase _createTaskUseCase;
  final DeleteTaskUseCase _deleteTaskUseCase;
  final ShowTaskUseCase _showTaskUseCase;

  TaskCubit({
    required FetchTasksUseCase fetchTasksUseCase,
    required UpdateTaskUseCase updateTaskUseCase,
    required CreateTaskUseCase createTaskUseCase,
    required DeleteTaskUseCase changeTaskStatusUseCase,
    required ShowTaskUseCase showTaskUseCase,
  })  : _deleteTaskUseCase = changeTaskStatusUseCase,
        _createTaskUseCase = createTaskUseCase,
        _updateTaskUseCase = updateTaskUseCase,
        _fetchTasksUseCase = fetchTasksUseCase,
        _showTaskUseCase = showTaskUseCase,
        super(const TaskState());

  resetPagination() {
    state.pagination.refreshCancelToken();
    emit(state.copyWith(
        pagination: PaginationInput(
            userId: instance<AppPreferences>().userPreferences.userData.id)));
  }

  Future<void> fetchTasks() async {
    resetPagination();
    emit(state.copyWith(
        status: CrudBlocStatus.loading,
        pagination: PaginationInput(
            userId: instance<AppPreferences>().userPreferences.userData.id)));
    try {
      final result = await Future.sync(
          () async => await _fetchTasksUseCase.execute(state.pagination));
      result.when(
        (tasks) {
          emit(state.copyWith(status: CrudBlocStatus.success, tasks: tasks));
        },
        (error) {
          emit(state.copyWith(
              status: CrudBlocStatus.error, errorMessage: error.message));
        },
      );
    } catch (e) {
      emit(state.copyWith(
          status: CrudBlocStatus.error, errorMessage: e.toString()));
    }
  }

  Future<void> loadMoreTasks() async {
    state.pagination.refreshCancelToken();
    emit(state.copyWith(
        status: CrudBlocStatus.loadingPagination,
        pagination:
            state.pagination.copyWith(page: state.pagination.page + 1)));
    try {
      final result = await _fetchTasksUseCase.execute(state.pagination);
      result.when(
        (tasks) {
          emit(state.copyWith(
              status: CrudBlocStatus.success,
              tasks: List.from([...state.tasks, ...tasks])));
        },
        (error) {
          emit(state.copyWith(
              status: CrudBlocStatus.error,
              errorMessage: error.message,
              pagination:
                  state.pagination.copyWith(page: state.pagination.page - 1)));
        },
      );
    } catch (e) {
      emit(state.copyWith(
          status: CrudBlocStatus.error,
          errorMessage: e.toString(),
          pagination:
              state.pagination.copyWith(page: state.pagination.page - 1)));
    }
  }

  Future<void> createTask(Task task) async {
    emit(state.copyWith(status: CrudBlocStatus.creating));
    try {
      final result = await Future.sync(() async =>
          await _createTaskUseCase.execute(CreateTaskInput(
              completed: task.completed,
              todo: task.todo,
              userId: task.userId)));
      result.when(
        (createdTask) {
          final updatedTasks = List<Task>.from(state.tasks)..add(createdTask);
          emit(state.copyWith(
              status: CrudBlocStatus.createSuccess, tasks: updatedTasks));
        },
        (error) {
          emit(state.copyWith(
              status: CrudBlocStatus.createError, errorMessage: error.message));
        },
      );
    } catch (e) {
      emit(state.copyWith(
          status: CrudBlocStatus.createError, errorMessage: e.toString()));
    }
  }

  Future<void> updateTask(Task task) async {
    emit(state.copyWith(status: CrudBlocStatus.updating));
    try {
      final result = await Future.sync(() async =>
          await _updateTaskUseCase.execute(UpdateTaskInput(
              completed: task.completed, todo: task.todo, id: task.id)));
      result.when(
        (updatedTask) {
          final updatedTasks = state.tasks
              .map<Task>((c) => c.id == task.id ? updatedTask : c)
              .toList();
          emit(state.copyWith(
              status: CrudBlocStatus.updateSuccess, tasks: updatedTasks));
        },
        (error) {
          emit(state.copyWith(
              status: CrudBlocStatus.updateError, errorMessage: error.message));
        },
      );
    } catch (e) {
      emit(state.copyWith(
          status: CrudBlocStatus.updateError, errorMessage: e.toString()));
    }
  }

  Future<void> deleteTask(int id) async {
    emit(state.copyWith(status: CrudBlocStatus.deleting));
    try {
      final result =
          await Future.sync(() async => await _deleteTaskUseCase.execute(id));
      result.when(
        (_) {
          final updatedTasks = state.tasks.where((c) => c.id != id).toList();
          emit(state.copyWith(
              status: CrudBlocStatus.deleteSuccess, tasks: updatedTasks));
        },
        (error) {
          emit(state.copyWith(
              status: CrudBlocStatus.deleteError, errorMessage: error.message));
        },
      );
    } catch (e) {
      emit(state.copyWith(
          status: CrudBlocStatus.deleteError, errorMessage: e.toString()));
    }
  }

  Future<void> showTask(String id) async {
    emit(state.copyWith(status: CrudBlocStatus.loadingShow));
    try {
      final result = await Future.sync(
          () async => await _showTaskUseCase.execute(int.tryParse(id) ?? -1));
      result.when(
        (company) {
          emit(state.copyWith(
              status: CrudBlocStatus.showSuccess, currentShowTask: company));
        },
        (error) {
          emit(state.copyWith(
              status: CrudBlocStatus.showError, errorMessage: error.message));
        },
      );
    } catch (e) {
      emit(state.copyWith(
          status: CrudBlocStatus.showError, errorMessage: e.toString()));
    }
  }
}
