part of 'task_bloc.dart';

@freezed
class TaskState with _$TaskState {
  const factory TaskState({
    @Default([]) List<Task> tasks,
    Task? currentShowTask,
    @Default(PaginationInput()) PaginationInput pagination,
    @Default(CrudBlocStatus.initial) CrudBlocStatus status,
    String? errorMessage,
  }) = _TaskState;
}

enum CrudBlocStatus {
  initial,
  loading,
  loadingPagination,
  success,
  error,
  creating,
  createSuccess,
  createError,
  updating,
  updateSuccess,
  updateError,
  showError,
  loadingShow,
  showSuccess,
  deleting,
  deleteSuccess,
  deleteError;

  bool get isLoading => [
        CrudBlocStatus.loading,
        CrudBlocStatus.creating,
        CrudBlocStatus.deleting,
        CrudBlocStatus.updating,
      ].contains(this);

  bool get isSuccess => [
        CrudBlocStatus.createSuccess,
        CrudBlocStatus.deleteSuccess,
        CrudBlocStatus.updateSuccess,
      ].contains(this);

  bool get hasError => [
        CrudBlocStatus.error,
        CrudBlocStatus.createError,
        CrudBlocStatus.deleteError,
        CrudBlocStatus.showError,
        CrudBlocStatus.updateError
      ].contains(this);
}
