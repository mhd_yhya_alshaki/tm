import 'package:multiple_result/multiple_result.dart';
import 'package:tm/core/models/basic_input/pagination_input/pagination_input.dart';
import 'package:tm/core/models/basic_models/default_success_model/DefaultSuccessModel.dart';
import 'package:tm/core/models/basic_use_cases/basic_use_cases.dart';
import 'package:tm/core/models/failure_model/failure.dart';
import 'package:tm/features/task/domain/repositories/task_repository.dart';

import '../../entities/task_model/task.dart';

// Input Classes

class UpdateTaskInput {
  final String todo;
  final bool completed;
  final int id;

  UpdateTaskInput({
    required this.todo,
    required this.completed,
    required this.id,
  });
}

class CreateTaskInput {
  final String todo;
  final bool completed;
  final int userId;

  CreateTaskInput({
    required this.todo,
    required this.completed,
    required this.userId,
  });
}

// Use Case Classes
class FetchTasksUseCase implements BaseUseCase<PaginationInput, List<Task>> {
  final TaskRepository repository;

  FetchTasksUseCase(this.repository);

  @override
  Future<Result<List<Task>, FailureModel>> execute(
      PaginationInput input) async {
    return await repository.fetch(input);
  }
}

class UpdateTaskUseCase implements BaseUseCase<UpdateTaskInput, Task> {
  final TaskRepository repository;

  UpdateTaskUseCase(this.repository);

  @override
  Future<Result<Task, FailureModel>> execute(UpdateTaskInput input) {
    return repository.update(
      completed: input.completed,
      todo: input.todo,
      id: input.id,
    );
  }
}

class CreateTaskUseCase implements BaseUseCase<CreateTaskInput, Task> {
  final TaskRepository repository;

  CreateTaskUseCase(this.repository);

  @override
  Future<Result<Task, FailureModel>> execute(CreateTaskInput input) {
    return repository.create(
        userId: input.userId, todo: input.todo, completed: input.completed);
  }
}

class DeleteTaskUseCase implements BaseUseCase<int, DefaultSuccessModel> {
  final TaskRepository repository;

  DeleteTaskUseCase(this.repository);

  @override
  Future<Result<DefaultSuccessModel, FailureModel>> execute(int input) {
    return repository.delete(input);
  }
}

class ShowTaskUseCase implements BaseUseCase<int, Task> {
  final TaskRepository repository;

  ShowTaskUseCase(this.repository);

  @override
  Future<Result<Task, FailureModel>> execute(int input) {
    return repository.show(input);
  }
}
