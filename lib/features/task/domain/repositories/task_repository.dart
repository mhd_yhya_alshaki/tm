import 'package:multiple_result/multiple_result.dart';
import 'package:tm/core/models/basic_input/pagination_input/pagination_input.dart';
import 'package:tm/core/models/basic_models/default_success_model/DefaultSuccessModel.dart';

import '../../../../core/models/failure_model/failure.dart';
import '../../entities/task_model/task.dart';

abstract class TaskRepository {
  Future<Result<List<Task>, FailureModel>> fetch(PaginationInput pagination);

  Future<Result<Task, FailureModel>> update(
      {required String todo, required bool completed, required int id});

  Future<Result<Task, FailureModel>> create(
      {required String todo, required bool completed, required int userId});

  Future<Result<DefaultSuccessModel, FailureModel>> delete(int id);

  Future<Result<Task, FailureModel>> show(int id);

  Future<void> syncTasks();
}
