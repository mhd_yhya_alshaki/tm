import 'package:freezed_annotation/freezed_annotation.dart';

part 'task.freezed.dart';
part 'task.g.dart';

@unfreezed
class Task with _$Task {
  factory Task(
      {@Default(-1) int id,
      @Default('') String todo,
      @Default(false) bool completed,
      @Default(false) bool needToSync,
      @Default(-1) int userId}) = _Task;

  factory Task.fromJson(Map<String, dynamic> json) => _$TaskFromJson(json);
}
