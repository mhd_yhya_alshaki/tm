// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'task.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$TaskImpl _$$TaskImplFromJson(Map<String, dynamic> json) => _$TaskImpl(
      id: (json['id'] as num?)?.toInt() ?? -1,
      todo: json['todo'] as String? ?? '',
      completed: json['completed'] as bool? ?? false,
      needToSync: json['needToSync'] as bool? ?? false,
      userId: (json['userId'] as num?)?.toInt() ?? -1,
    );

Map<String, dynamic> _$$TaskImplToJson(_$TaskImpl instance) =>
    <String, dynamic>{
      'id': instance.id,
      'todo': instance.todo,
      'completed': instance.completed,
      'needToSync': instance.needToSync,
      'userId': instance.userId,
    };
