// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'todos_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$TodosResponseModelImpl _$$TodosResponseModelImplFromJson(
        Map<String, dynamic> json) =>
    _$TodosResponseModelImpl(
      todos: (json['todos'] as List<dynamic>)
          .map((e) => Task.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$TodosResponseModelImplToJson(
        _$TodosResponseModelImpl instance) =>
    <String, dynamic>{
      'todos': instance.todos,
    };
