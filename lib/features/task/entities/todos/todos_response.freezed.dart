// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'todos_response.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

TodosResponseModel _$TodosResponseModelFromJson(Map<String, dynamic> json) {
  return _TodosResponseModel.fromJson(json);
}

/// @nodoc
mixin _$TodosResponseModel {
  List<Task> get todos => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $TodosResponseModelCopyWith<TodosResponseModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TodosResponseModelCopyWith<$Res> {
  factory $TodosResponseModelCopyWith(
          TodosResponseModel value, $Res Function(TodosResponseModel) then) =
      _$TodosResponseModelCopyWithImpl<$Res, TodosResponseModel>;
  @useResult
  $Res call({List<Task> todos});
}

/// @nodoc
class _$TodosResponseModelCopyWithImpl<$Res, $Val extends TodosResponseModel>
    implements $TodosResponseModelCopyWith<$Res> {
  _$TodosResponseModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? todos = null,
  }) {
    return _then(_value.copyWith(
      todos: null == todos
          ? _value.todos
          : todos // ignore: cast_nullable_to_non_nullable
              as List<Task>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$TodosResponseModelImplCopyWith<$Res>
    implements $TodosResponseModelCopyWith<$Res> {
  factory _$$TodosResponseModelImplCopyWith(_$TodosResponseModelImpl value,
          $Res Function(_$TodosResponseModelImpl) then) =
      __$$TodosResponseModelImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({List<Task> todos});
}

/// @nodoc
class __$$TodosResponseModelImplCopyWithImpl<$Res>
    extends _$TodosResponseModelCopyWithImpl<$Res, _$TodosResponseModelImpl>
    implements _$$TodosResponseModelImplCopyWith<$Res> {
  __$$TodosResponseModelImplCopyWithImpl(_$TodosResponseModelImpl _value,
      $Res Function(_$TodosResponseModelImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? todos = null,
  }) {
    return _then(_$TodosResponseModelImpl(
      todos: null == todos
          ? _value._todos
          : todos // ignore: cast_nullable_to_non_nullable
              as List<Task>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$TodosResponseModelImpl implements _TodosResponseModel {
  const _$TodosResponseModelImpl({required final List<Task> todos})
      : _todos = todos;

  factory _$TodosResponseModelImpl.fromJson(Map<String, dynamic> json) =>
      _$$TodosResponseModelImplFromJson(json);

  final List<Task> _todos;
  @override
  List<Task> get todos {
    if (_todos is EqualUnmodifiableListView) return _todos;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_todos);
  }

  @override
  String toString() {
    return 'TodosResponseModel(todos: $todos)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$TodosResponseModelImpl &&
            const DeepCollectionEquality().equals(other._todos, _todos));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_todos));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$TodosResponseModelImplCopyWith<_$TodosResponseModelImpl> get copyWith =>
      __$$TodosResponseModelImplCopyWithImpl<_$TodosResponseModelImpl>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$TodosResponseModelImplToJson(
      this,
    );
  }
}

abstract class _TodosResponseModel implements TodosResponseModel {
  const factory _TodosResponseModel({required final List<Task> todos}) =
      _$TodosResponseModelImpl;

  factory _TodosResponseModel.fromJson(Map<String, dynamic> json) =
      _$TodosResponseModelImpl.fromJson;

  @override
  List<Task> get todos;
  @override
  @JsonKey(ignore: true)
  _$$TodosResponseModelImplCopyWith<_$TodosResponseModelImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
