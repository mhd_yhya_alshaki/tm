import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:tm/features/task/entities/task_model/task.dart';

part 'todos_response.freezed.dart';
part 'todos_response.g.dart';

@freezed
class TodosResponseModel with _$TodosResponseModel {
  const factory TodosResponseModel({required List<Task> todos}) =
      _TodosResponseModel;

  factory TodosResponseModel.fromJson(Map<String, dynamic> json) =>
      _$TodosResponseModelFromJson(json);
}
