import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:tm/app/dependency_injection.dart';
import 'package:tm/core/router/routes_manager.dart';
import 'package:tm/core/utils/helpers/helper.dart';
import 'package:tm/features/task/bloc/task_bloc.dart';
import 'package:tm/widgets/custom_containers/basic_container.dart';
import 'package:tm/widgets/loading_widgets/willpop_scope_loading.dart';
import 'package:tm/widgets/slidable_action.dart';

import '../entities/task_model/task.dart';

class TaskCard extends StatelessWidget {
  final Task task;

  const TaskCard({Key? key, required this.task}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SlidableAction(
      onEdit: () {
        appRouter.goNamed(RoutesNames.crudTaskRoute,
            queryParameters: {'id': task.id.toString()});
      },
      onDelete: () {
        instance<TaskCubit>().deleteTask(task.id);
        LoadingDialog.show(context);
      },
      child: BasicContainer(
        padding: Helper.instance.paddingHelper.mediumSymmetricPadding,
        child: ListTile(
          dense: false,
          leading: Container(
            height: 10.h,
            width: 10.w,
            decoration: BoxDecoration(
                color: task.completed ? Colors.green : Colors.yellow,
                shape: BoxShape.circle),
          ),
          title: Text(task.todo),
          // isThreeLine: true,
          onTap: () {
            // Handle onTap event if needed
          },
        ),
      ),
    );
  }
}
