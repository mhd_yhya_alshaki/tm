import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:multiple_result/multiple_result.dart';
import 'package:tm/core/models/basic_input/pagination_input/pagination_input.dart';
import 'package:tm/core/models/basic_models/default_success_model/DefaultSuccessModel.dart';
import 'package:tm/core/utils/local_storage/task/task_preferences.dart';
import 'package:tm/features/task/domain/repositories/task_repository.dart';

import '../../../../core/models/failure_model/failure.dart';
import '../../../../core/network/network_info.dart';
import '../../../../generated/locale_keys.g.dart';
import '../../entities/task_model/task.dart';
import '../datasources/task_datasource.dart';

class TaskRepositoryImplementation implements TaskRepository {
  final TaskDatasource datasource;
  final NetworkInfo _networkInfo;
  final TaskPreferences taskPreferences;

  TaskRepositoryImplementation(
      this.datasource, this._networkInfo, this.taskPreferences);

  @override
  Future<void> syncTasks() async {
    if (await _networkInfo.isConnected) {
      // Sync local tasks
      List<Task> localTasks = await taskPreferences.getTasks();
      for (var task in localTasks) {
        if (task.id < 0) {
          // This is a new task that hasn't been synced with the server
          final result = await create(
              todo: task.todo, completed: task.completed, userId: task.userId);
          if (result.isSuccess()) {
            // Update the local task with the new ID from the server
            if (result.tryGetSuccess() != null) {
              await taskPreferences.updateTask(
                  (result.tryGetSuccess() as Task?)!
                      .copyWith(needToSync: false));
            }
          }
        } else if (task.needToSync) {
          // This is an existing task that might need updating
          await update(todo: task.todo, completed: task.completed, id: task.id);
        }
      }

      // Sync deleted tasks
      List<int> deletedTasks = await taskPreferences.getDeletedTasks();
      for (var taskId in deletedTasks) {
        await delete(taskId);
      }

      // Clear deleted tasks list after sync
      await taskPreferences.clearDeletedTasks();
    }
  }

  @override
  Future<Result<List<Task>, FailureModel>> fetch(
      PaginationInput pagination) async {
    if (await _networkInfo.isConnected) {
      try {
        final response = await datasource.fetch(
            limit: pagination.perPage,
            skip: (pagination.page - 1) * pagination.perPage);
        if (response.response.statusCode == 200 ||
            response.response.statusCode == 201) {
          // Save fetched tasks to local storage
          for (var task in response.data.todos) {
            await taskPreferences.saveTask(task);
          }
          return Success(response.data.todos);
        } else {
          return Error(FailureModel.fromJson(response.response.data));
        }
      } on DioException catch (e) {
        return _handleDioException(e);
      }
    } else {
      try {
        final tasks = await taskPreferences.getTasks();
        return Success(tasks);
      } catch (e) {
        return Error(
            FailureModel(message: LocaleKeys.dioError_noInternetError.tr()));
      }
    }
  }

  @override
  Future<Result<Task, FailureModel>> update(
      {required String todo, required bool completed, required int id}) async {
    if (await _networkInfo.isConnected) {
      try {
        final response =
            await datasource.update(completed: completed, todo: todo, id: id);
        if (response.response.statusCode == 200 ||
            response.response.statusCode == 201) {
          await taskPreferences
              .updateTask(response.data.copyWith(needToSync: false));
          return Result.success(response.data);
        } else {
          return Result.error(FailureModel.fromJson(response.response.data));
        }
      } on DioException catch (e) {
        return _handleDioException(e);
      }
    } else {
      final task = Task(
          id: id,
          todo: todo,
          completed: completed,
          userId: -1,
          needToSync: true);
      await taskPreferences.updateTask(task);
      return Result.success(task);
    }
  }

  @override
  Future<Result<Task, FailureModel>> create(
      {required String todo,
      required bool completed,
      required int userId}) async {
    if (await _networkInfo.isConnected) {
      try {
        final response = await datasource.create(
            completed: completed, todo: todo, userId: userId);
        if (response.response.statusCode == 200 ||
            response.response.statusCode == 201) {
          await taskPreferences.saveTask(response.data);
          return Result.success(response.data);
        } else {
          return Result.error(FailureModel.fromJson(response.response.data));
        }
      } on DioException catch (e) {
        return _handleDioException(e);
      }
    } else {
      final task = Task(
          id: DateTime.now().millisecondsSinceEpoch * -1,
          todo: todo,
          completed: completed,
          userId: userId);
      await taskPreferences.saveTask(task);
      return Result.success(task);
    }
  }

  @override
  Future<Result<DefaultSuccessModel, FailureModel>> delete(int id) async {
    if (await _networkInfo.isConnected) {
      try {
        final response = await datasource.delete(id);
        if (response.response.statusCode == 200 ||
            response.response.statusCode == 201) {
          await taskPreferences.deleteTask(id);
          return Result.success(response.data);
        } else {
          return Result.error(FailureModel.fromJson(response.response.data));
        }
      } on DioException catch (e) {
        return _handleDioException(e);
      }
    } else {
      await taskPreferences.deleteTask(id);
      return const Result.success(DefaultSuccessModel(success: 'success'));
    }
  }

  @override
  Future<Result<Task, FailureModel>> show(int id) async {
    if (await _networkInfo.isConnected) {
      try {
        final response = await datasource.show(id);
        if (response.response.statusCode == 200 ||
            response.response.statusCode == 201) {
          return Result.success(response.data);
        } else {
          return Result.error(FailureModel.fromJson(response.response.data));
        }
      } on DioException catch (e) {
        return _handleDioException(e);
      }
    } else {
      try {
        final task = await taskPreferences.getTask(id);
        return Result.success(task);
      } catch (e) {
        return Result.error(
            FailureModel(message: LocaleKeys.dioError_noInternetError.tr()));
      }
    }
  }

  Result<T, FailureModel> _handleDioException<T>(DioException e) {
    if (e.type == DioExceptionType.cancel) {
      return Result.error(
          FailureModel(message: DioExceptionType.cancel.toString()));
    }
    return Result.error(FailureModel.fromJson(
        e.response?.data ?? {'message': 'Unknown error occurred'}));
  }
}
