import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';
import 'package:tm/core/models/basic_models/default_success_model/DefaultSuccessModel.dart';
import 'package:tm/features/task/data/task_endpoints/task_endpoints.dart';
import 'package:tm/features/task/entities/todos/todos_response.dart';

import '../../../../core/constants/constant.dart';
import '../../entities/task_model/task.dart';

part 'task_datasource.g.dart';

@RestApi(baseUrl: Constants.apiUrl)
abstract class TaskDatasource {
  factory TaskDatasource(Dio dio, {String baseUrl}) = _TaskDatasource;

  @GET(TaskEndPoints.tasks)
  Future<HttpResponse<TodosResponseModel>> fetch({
    @Query('limit') int? limit,
    @Query('skip') int? skip,
  });

  @PUT(TaskEndPoints.updateTasks)
  Future<HttpResponse<Task>> update(
      {@Field() required String todo,
      @Field() required bool completed,
      @Path() required int id});

  @POST(TaskEndPoints.addTask)
  Future<HttpResponse<Task>> create({
    @Field() required String todo,
    @Field() required bool completed,
    @Field() required int userId,
  });

  @DELETE(TaskEndPoints.deleteTasks)
  Future<HttpResponse<DefaultSuccessModel>> delete(@Path() int id);

  @GET(TaskEndPoints.task)
  Future<HttpResponse<Task>> show(@Path() int id);
}
