class TaskEndPoints {
  static const String tasks = 'todos';
  static const String task = 'todos/{id}';
  static const String updateTasks = 'todos/{id}';
  static const String deleteTasks = 'todos/{id}';
  static const String addTask = 'todos/add';
}
