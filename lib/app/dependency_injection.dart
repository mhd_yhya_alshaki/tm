import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tm/core/network/network_info.dart';
import 'package:tm/features/task/bloc/task_bloc.dart';
import 'package:tm/features/task/data/datasources/task_datasource.dart';
import 'package:tm/features/task/data/repositories/task_repository_implementation.dart';
import 'package:tm/features/task/domain/repositories/task_repository.dart';
import 'package:tm/features/task/domain/usecases/task.dart';

import '../core/network/dio_factory.dart';
import '../core/utils/local_storage/app_preferences.dart';
import '../features/auth/data/datasources/auth_datasource.dart';
import '../features/auth/data/repositories/auth_repository_implementation.dart';
import '../features/auth/domain/repositories/auth_repository.dart';
import '../features/auth/domain/usecases/auth.dart';

final instance = GetIt.instance;

Future<void> initAppModule() async {
  final sharedPreferences = await SharedPreferences.getInstance();
  instance.registerFactory<SharedPreferences>(() => sharedPreferences);
  instance.registerFactory<AppPreferences>(() => AppPreferences(instance()));
  instance.registerSingleton<DioFactory>(DioFactory(instance()));
  instance.registerLazySingleton<NetworkInfo>(
      () => NetworkInfoImplementer(Connectivity()));
  var dio = await instance<DioFactory>().getDio();
  initAuth(dio);
  registerRepository(instance, dio);
  registerUseCases(instance);
  registerBloc(instance);
}

void registerUseCases(GetIt instance) {
  // region Task
  instance.registerLazySingleton<FetchTasksUseCase>(
      () => FetchTasksUseCase(instance<TaskRepository>()));
  instance.registerLazySingleton<UpdateTaskUseCase>(
      () => UpdateTaskUseCase(instance<TaskRepository>()));
  instance.registerLazySingleton<CreateTaskUseCase>(
      () => CreateTaskUseCase(instance<TaskRepository>()));
  instance.registerLazySingleton<DeleteTaskUseCase>(
      () => DeleteTaskUseCase(instance<TaskRepository>()));
  instance.registerLazySingleton<ShowTaskUseCase>(
      () => ShowTaskUseCase(instance<TaskRepository>()));
  //endregion
}

Future<void> registerRepository(GetIt instance, dio) async {
  //region  Task
  // Register Data Source
  instance.registerLazySingleton<TaskDatasource>(() => TaskDatasource(dio));
  // Register Repository
  instance.registerLazySingleton<TaskRepository>(() =>
      TaskRepositoryImplementation(instance<TaskDatasource>(),
          instance<NetworkInfo>(), instance<AppPreferences>().taskPreferences));
  //endregion
}

void registerBloc(GetIt instance) {
  instance.registerLazySingleton(() => TaskCubit(
        fetchTasksUseCase: instance(),
        updateTaskUseCase: instance(),
        createTaskUseCase: instance(),
        changeTaskStatusUseCase: instance(),
        showTaskUseCase: instance(),
      ));
}

initAuth(dio) {
  //region auth
  instance.registerLazySingleton<AuthDatasource>(() => AuthDatasource(dio));
  instance.registerLazySingleton<AuthRepository>(
      () => AuthRepositoryImplementation(instance(), instance()));
  instance.registerLazySingleton<LoginUseCase>(() => LoginUseCase(instance()));
  //endregion
}
