import 'dart:ui';

import 'package:animated_theme_switcher/animated_theme_switcher.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../core/utils/local_storage/app_preferences.dart';
import '../core/utils/resources/resources.dart';
import '../generated/locale_keys.g.dart';
import 'dependency_injection.dart';

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    Size size = const Size(360, 690);
    return ScreenUtilInit(
        designSize: size,
        minTextAdapt: true,
        splitScreenMode: true,
        builder: (secondContext, child) => ThemeProvider(
            initTheme:
                instance<AppPreferences>().themePreferences.getTheme(context),
            builder: (p0, theme) => MaterialApp.router(
                  title: LocaleKeys.appName.tr(),
                  localizationsDelegates: context.localizationDelegates,
                  supportedLocales: context.supportedLocales,
                  locale: context.locale,
                  scrollBehavior: const MaterialScrollBehavior().copyWith(
                    dragDevices: {
                      PointerDeviceKind.mouse,
                      PointerDeviceKind.touch,
                    },
                  ),
                  // themeMode: theme.isDarkTheme ? ThemeMode.dark : ThemeMode.light,
                  themeMode: ThemeMode.light,
                  theme: instance<AppPreferences>()
                      .themePreferences
                      .getTheme(context),
                  darkTheme: instance<AppPreferences>()
                      .themePreferences
                      .getTheme(context),
                  routerConfig: appRouter,
                  debugShowCheckedModeBanner: false,
                )));
  }
}
