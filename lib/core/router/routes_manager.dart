import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:tm/features/auth/presentation/auth_view.dart';
import 'package:tm/features/home/presentation/home_page.dart';
import 'package:tm/features/task/presentation/crud_task_page.dart';

import '../../app/dependency_injection.dart';
import '../router/router_observer.dart';
import '../utils/local_storage/app_preferences.dart';

final rootNavigatorKey = GlobalKey<NavigatorState>();

final _sectionNavigatorKey = GlobalKey<NavigatorState>();
// final shellNavigatorKey = GlobalKey<NavigatorState>();

class RoutesNames {
  static const String splashRoute = 'splash';
  static const String loginRoute = 'login';
  static const String homeRoute = '/Home';
  static const String crudTaskRoute = 'CrudTask';
}

class RoutesPaths {
  static String homeRoute = '/Home';
  static const String splashRoute = '/splash';
  static const String loginRoute = '/login';
  static const String crudTaskRoute = 'CrudTask';
}

late StatefulNavigationShell appNavigationShell;
final GoRouter appRouter = GoRouter(
  initialLocation: RoutesPaths.homeRoute,
  navigatorKey: rootNavigatorKey,
  observers: [MyRouteObserver()],
  redirect: (context, state) async {
    var appPref = instance<AppPreferences>();
    final loggedIn = appPref.userPreferences.isUserLoggedIn();
    final isLoginPage = state.uri.path.contains(RoutesPaths.loginRoute);
    if (!loggedIn && !isLoginPage) {
      return RoutesPaths.loginRoute;
    } else if (loggedIn && isLoginPage) {
      return RoutesPaths.homeRoute;
    }
    return null;
  },
  routes: [
    GoRoute(
        name: RoutesNames.splashRoute,
        path: RoutesPaths.splashRoute,
        builder: (context, state) => const Scaffold(
              body: Text("Home"),
            )),
    GoRoute(
        name: RoutesNames.homeRoute,
        path: RoutesPaths.homeRoute,
        builder: (context, state) => const HomePage(),
        routes: [
          GoRoute(
              name: RoutesNames.crudTaskRoute,
              path: RoutesPaths.crudTaskRoute,
              builder: (context, state) => CrudTaskPage(
                    id: state.uri.queryParameters['id'],
                  )),
        ]),
    GoRoute(
        name: RoutesNames.loginRoute,
        path: RoutesPaths.loginRoute,
        builder: (context, state) => const AuthPage()),
  ],
);

class FadeTransitionPage extends CustomTransitionPage<void> {
  /// Creates a [FadeTransitionPage].
  FadeTransitionPage({
    required LocalKey super.key,
    required super.child,
  }) : super(
            transitionsBuilder: (BuildContext context,
                    Animation<double> animation,
                    Animation<double> secondaryAnimation,
                    Widget child) =>
                FadeTransition(
                  opacity: animation.drive(_curveTween),
                  child: child,
                ));

  static final CurveTween _curveTween = CurveTween(curve: Curves.easeIn);
}
