enum ResponseStatusCode {
  unAuthenticated;

  get value {
    switch (this) {
      case ResponseStatusCode.unAuthenticated:
        return 'E004';
    }
  }
}
