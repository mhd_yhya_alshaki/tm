import 'dart:convert';

import 'package:dio/dio.dart';

import '../../app/dependency_injection.dart';
import '../network/response_status_code.dart';
import '../router/routes_manager.dart';
import '../utils/helpers/helper.dart';
import '../utils/local_storage/app_preferences.dart';

class GeneralInterceptor extends Interceptor {
  @override
  Future<void> onRequest(
      RequestOptions options, RequestInterceptorHandler handler) async {
    // logNetwork(
    //     message:
    //         'HTTPs method => ${options.method} \n'
    //             'Request => ${options.baseUrl}${options.path}${options.queryParameters} \n'
    //             'Header  => ${options.headers}',
    //     level: LogLevel.info);
    // if (kDebugMode) {
    //   log(options.data.toString());
    // }
    super.onRequest(options, handler);
  }

  @override
  Future<void> onResponse(
      Response response, ResponseInterceptorHandler handler) async {
    try {
      // logNetwork(
      //     message:
      //     'Response => StatusCode: ${response.statusCode} \n'
      //         'Response => Body: ${response.data} \n'
      //         'Response => Header: ${response.headers.map}',
      //     level: LogLevel.info);
      // if (kDebugMode) {
      //   log(response.requestOptions.data.toString());
      // }
      if (response.data['code'] == ResponseStatusCode.unAuthenticated.value) {
        AppPreferences appPreferences = instance<AppPreferences>();
        await appPreferences.userPreferences.logOutPref();
        while (appRouter.canPop()) {
          appRouter.pop();
          appRouter.refresh();
        }
        // RouteManager().appRouter.goNamed(RoutesNames.loginRoute);
      }
    } catch (error, stacktrace) {
      // logCatchDebug(message: 'Catch Error',error: error,stackTrace: stacktrace);
    }
    return handler.next(response);
  }

  @override
  Future<void> onError(
      DioException err, ErrorInterceptorHandler handler) async {
    try {
      if (err.response?.data['code'] ==
          ResponseStatusCode.unAuthenticated.value) {
        Helper.instance.routerHelper.logoutWithoutApi();
        // logNetwork(message: 'Error: ${err.error}, Message: ${err.message}',level: LogLevel.error,error: err);
      }
    } catch (error, stacktrace) {
      // logCatchDebug(message: 'Catch Error',error: error,stackTrace: stacktrace);
    }
    // if (kDebugMode) {
    //   print(_cURLRepresentation(err.requestOptions));
    // }

    return handler.next(err);
  }

  String _cURLRepresentation(RequestOptions options) {
    List<String> components = ['curl -i'];
    if (options.method.toUpperCase() != 'GET') {
      components.add('-X ${options.method}');
    }

    options.headers.forEach((k, v) {
      if (k != 'Cookie') {
        components.add('-H "$k: $v"');
      }
    });

    if (options.data != null) {
      // FormData can't be JSON-serialized, so keep only their fields attributes
      if (options.data is FormData) {
        options.data = Map.fromEntries(options.data.fields);
      }
      final data = json.encode(options.data).replaceAll('"', '\\"');
      components.add('-d "$data"');
    }

    components.add('"${options.uri.toString()}"');

    return components.join(' \\\n\t');
  }
}
