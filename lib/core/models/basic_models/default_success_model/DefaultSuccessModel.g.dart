// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'DefaultSuccessModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$DefaultSuccessModelImpl _$$DefaultSuccessModelImplFromJson(
        Map<String, dynamic> json) =>
    _$DefaultSuccessModelImpl(
      success: json['success'] as String? ?? '',
      code: json['code'] as String? ?? '',
    );

Map<String, dynamic> _$$DefaultSuccessModelImplToJson(
        _$DefaultSuccessModelImpl instance) =>
    <String, dynamic>{
      'success': instance.success,
      'code': instance.code,
    };
