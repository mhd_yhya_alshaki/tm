import 'package:dio/dio.dart';
import 'package:tm/core/constants/constant.dart';

class PaginationInput {
  final int page;
  final int perPage;
  final String? from_date;
  final String? to_date;
  final String? search;
  final int? id;
  final int? userId;
  final CancelToken? cancelToken;
  final Map<String, dynamic> extra;

  const PaginationInput(
      {this.page = 1,
      this.cancelToken,
      this.userId,
      this.extra = const {},
      this.perPage = Constants.perPage,
      this.from_date,
      this.to_date,
      this.id,
      this.search});

  PaginationInput refreshCancelToken() {
    cancelToken?.cancel();
    return copyWith(cancelToken: CancelToken());
  }

  PaginationInput copyWith({
    int? page,
    int? perPage,
    String? fromDate,
    String? toDate,
    String? search,
    int? id,
    CancelToken? cancelToken,
    Map<String, dynamic>? extra,
  }) {
    return PaginationInput(
      page: page ?? this.page,
      perPage: perPage ?? this.perPage,
      from_date: fromDate ?? this.from_date,
      to_date: toDate ?? this.to_date,
      search: search ?? this.search,
      id: id ?? this.id,
      cancelToken: cancelToken ?? this.cancelToken,
      extra: extra ?? this.extra,
    );
  }
}

class SearchInput {
  final CancelToken cancelToken;
  final String? value;

  SearchInput({
    required this.cancelToken,
    this.value,
  });
}
