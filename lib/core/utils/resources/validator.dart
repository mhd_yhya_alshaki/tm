class Validator {
  static const String passwordRegex =
      r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?\d)(?=.*?[#.?!@$%^&*-]).{8,}$';
  static const String codeRegex = r'^[A-Za-z]{3}[0-9]{5}$';
  static const String emailRegex = r'^[\w\.-]+@[\w\.-]+\.\w+$';
  static const String phoneRegex = r'^[+]{1}\d{9,15}$';

  static const String requiredField = '*';

  static String? validateRequiredFiled(String? value) =>
      (value?.trim().isEmpty ?? true) ? requiredField : null;

  static String? validateRequiredList(List? value) =>
      (value?.isEmpty ?? true) ? requiredField : null;

  static String? validateRequiredRegionNumber(
          {String? value, required int max, int? min}) =>
      (value?.trim().isEmpty ?? true)
          ? requiredField
          : (int.tryParse(value ?? '') ?? 0) > max
              ? 'Maximum Value is $max'
              : (int.tryParse(value ?? '') ?? 0) <
                      (min ?? (int.tryParse(value ?? '0') ?? 0))
                  ? 'Minimum Value is $min'
                  : null;

  static String? validateRequiredMinimumNumber({String? value, int? min}) =>
      (value?.trim().isEmpty ?? true)
          ? requiredField
          : (int.tryParse(value ?? '') ?? 0) <
                  (min ?? (int.tryParse(value ?? '0') ?? 0))
              ? 'Minimum Value is $min'
              : null;

  static String? validateRequiredObjectByID(int? id) =>
      ((id ?? -1) < 0) ? requiredField : null;

  static String? validateTextEql(String? value, String value2) =>
      (value == null || value.isEmpty)
          ? requiredField
          : (value == value2)
              ? 'please enter new value'
              : null;

  static String? userPasswordValidation(String? value) {
    String empty = validateRequiredFiled(value) ?? '';
    if (empty.isNotEmpty) {
      return empty;
    }
    final regExp = RegExp(passwordRegex);
    return regExp.hasMatch((value ?? '').trim()) ? null : 'Wrong password';
  }

  static String? emailValidation(String? value) {
    String empty = validateRequiredFiled(value) ?? '';
    if (empty.isNotEmpty) {
      return empty;
    }
    final regExp = RegExp(emailRegex);
    return regExp.hasMatch((value ?? '').trim()) ? null : 'Wrong Email';
  }

  static String? phoneValidation(String? value) {
    String empty = validateRequiredFiled(value) ?? '';
    if (empty.isNotEmpty) {
      return empty;
    }
    final regExp = RegExp(phoneRegex);
    return regExp.hasMatch((value ?? '').trim()) ? null : 'Wrong Phone';
  }

  static String? duplicatedValueRequired({List? list, String? value}) {
    String empty = validateRequiredFiled(value) ?? '';
    if (empty.isNotEmpty) {
      return empty;
    }
    return ((list ?? [])
                .where(
                  (element) => element == value,
                )
                .toList()
                .length >
            1)
        ? 'Duplicated'
        : null;
  }

  static String? duplicatedValueLisBeforAddtRequired(
      {List? list, String? value}) {
    String empty = validateRequiredFiled(value) ?? '';
    if (empty.isNotEmpty) {
      return empty;
    }
    return ((list ?? [])
            .where(
              (element) => element == value,
            )
            .toList()
            .isNotEmpty)
        ? 'Duplicated'
        : null;
  }

  static String? duplicatedValue({List? list, String? value}) {
    return ((list ?? [])
                .where(
                  (element) => element == value,
                )
                .toList()
                .length >
            1)
        ? 'Duplicated'
        : null;
  }

  static String? emailDuplicatedValue({List? list, String? value}) {
    String emailValid = emailValidation(value) ?? '';
    if (emailValid.isNotEmpty) {
      return emailValid;
    }
    return ((list ?? [])
                .where(
                  (element) => element == value,
                )
                .toList()
                .length >
            1)
        ? 'Duplicated'
        : null;
  }

  static String? userCodeValidation(String? value) {
    String empty = validateRequiredFiled(value) ?? '';
    if (empty.isNotEmpty) {
      return empty;
    }
    final regExp = RegExp(codeRegex);
    return regExp.hasMatch((value ?? '').trim()) ? null : 'Wrong code';
  }
}
