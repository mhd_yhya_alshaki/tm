import 'package:flutter_screenutil/flutter_screenutil.dart';

class AppSizeH {
  static double get s0 => 0.0.h;

  static double get s1 => 1.0.h;

  static double get s1_1 => 1.1.h;

  static double get s1_2 => 1.2.h;

  static double get s1_4 => 1.4.h;

  static double get s1_5 => 1.5.h;

  static double get s1_7 => 1.7.h;

  static double get s1_8 => 1.8.h;

  static double get s1_95 => 1.95.h;

  static double get s2 => 2.0.h;

  static double get s3 => 3.0.h;

  static double get s4 => 4.0.h;

  static double get s4_25 => 4.25.h;

  static double get s5 => 5.0.h;

  static double get s6 => 6.0.h;

  static double get s7 => 7.0.h;

  static double get s8 => 8.0.h;

  static double get s8_5 => 8.5.h;

  static double get s9 => 9.0.h;

  static double get s9_5 => 9.5.h;

  static double get s10 => 10.0.h;

  static double get s11 => 11.0.h;

  static double get s12 => 12.0.h;

  static double get s13 => 13.0.h;

  static double get s13_5 => 13.5.h;

  static double get s14 => 14.0.h;

  static double get s15 => 15.0.h;

  static double get s16 => 16.0.h;

  static double get s17 => 17.0.h;

  static double get s17_5 => 17.5.h;

  static double get s18 => 18.0.h;

  static double get s18_5 => 18.5.h;

  static double get s19 => 19.0.h;

  static double get s19_5 => 19.5.h;

  static double get s20 => 20.0.h;

  static double get s20_5 => 20.5.h;

  static double get s21 => 21.0.h;

  static double get s22 => 22.0.h;

  static double get s22_5 => 22.5.h;

  static double get s23 => 23.0.h;

  static double get s24 => 24.0.h;

  static double get s25 => 25.0.h;

  static double get s26 => 26.0.h;

  static double get s27 => 27.0.h;

  static double get s28 => 28.0.h;

  static double get s28_5 => 28.5.h;

  static double get s30 => 30.0.h;

  static double get s31 => 31.0.h;

  static double get s31_5 => 31.5.h;

  static double get s32 => 32.0.h;

  static double get s33 => 33.0.h;

  static double get s34 => 34.0.h;

  static double get s35 => 35.0.h;

  static double get s36 => 36.0.h;

  static double get s37_5 => 37.5.h;

  static double get s38 => 38.0.h;

  static double get s40 => 40.0.h;

  static double get s41 => 41.0.h;

  static double get s42 => 42.0.h;

  static double get s43 => 43.0.h;

  static double get s44 => 44.0.h;

  static double get s45 => 45.0.h;

  static double get s46 => 46.0.h;

  static double get s48 => 48.0.h;

  static double get s49 => 49.0.h;

  static double get s50 => 50.0.h;

  static double get s51 => 51.0.h;

  static double get s52 => 52.0.h;

  static double get s54 => 54.0.h;

  static double get s55 => 55.0.h;

  static double get s56 => 55.0.h;

  static double get s57 => 57.0.h;

  static double get s58 => 58.0.h;

  static double get s60 => 60.0.h;

  static double get s62 => 62.0.h;

  static double get s64 => 64.0.h;

  static double get s65 => 65.0.h;

  static double get s68 => 68.0.h;

  static double get s70 => 70.0.h;

  static double get s71 => 71.0.h;

  static double get s72 => 72.0.h;

  static double get s75 => 75.0.h;

  static double get s80 => 80.0.h;

  static double get s88 => 88.0.h;

  static double get s90 => 90.0.h;

  static double get s95 => 95.0.h;

  static double get s96 => 96.0.h;

  static double get s100 => 100.0.h;

  static double get s103 => 103.0.h;

  static double get s104 => 104.0.h;

  static double get s105 => 105.0.h;

  static double get s110 => 110.0.h;

  static double get s112 => 112.0.h;

  static double get s113 => 113.0.h;

  static double get s115 => 115.0.h;

  static double get s119 => 119.0.h;

  static double get s120 => 120.0.h;

  static double get s125 => 125.0.h;

  static double get s126 => 126.0.h;

  static double get s128 => 128.0.h;

  static double get s130 => 130.0.h;

  static double get s131 => 131.0.h;

  static double get s136 => 136.0.h;

  static double get s140 => 140.0.h;

  static double get s144 => 144.0.h;

  static double get s147 => 147.0.h;

  static double get s148 => 148.0.h;

  static double get s150 => 150.0.h;

  static double get s152 => 152.0.h;

  static double get s156 => 156.0.h;

  static double get s157 => 157.0.h;

  static double get s160 => 160.0.h;

  static double get s164 => 164.0.h;

  static double get s165 => 165.0.h;

  static double get s167 => 167.0.h;

  static double get s170 => 170.0.h;

  static double get s180 => 180.0.h;

  static double get s182 => 182.0.h;

  static double get s185 => 185.0.h;

  static double get s188 => 188.0.h;

  static double get s190 => 190.0.h;

  static double get s197 => 197.0.h;

  static double get s200 => 200.0.h;

  static double get s205 => 205.0.h;

  static double get s210 => 210.0.h;

  static double get s219 => 219.0.h;

  static double get s223 => 223.0.h;

  static double get s238 => 238.0.h;

  static double get s245 => 240.0.h;

  static double get s250 => 250.0.h;

  static double get s257_394 => 257.394.h;

  static double get s263 => 263.0.h;

  static double get s273 => 273.0.h;

  static double get s289 => 289.0.h;

  static double get s290 => 290.0.h;

  static double get s296 => 296.0.h;

  static double get s304 => 304.0.h;

  static double get s310 => 310.0.h;

  static double get s312 => 312.0.h;

  static double get s315 => 315.0.h;

  static double get s320 => 320.0.h;

  static double get s316 => 316.0.h;

  static double get s326 => 326.0.h;

  static double get s333 => 333.0.h;

  static double get s340 => 340.0.h;

  static double get s354 => 354.0.h;

  static double get s384 => 384.0.h;

  static double get s395 => 395.0.h;

  static double get s400 => 400.0.h;

  static double get s408 => 408.0.h;

  static double get s428 => 428.0.h;

  static double get s440 => 440.0.h;

  static double get s447 => 447.0.h;

  static double get s460 => 460.0.h;

  static double get s470 => 470.0.h;

  static double get s465 => 465.0.h;

  static double get s480 => 480.0.h;

  static double get s490 => 490.0.h;

  static double get s500 => 500.0.h;

  static double get s530 => 530.0.h;

  static double get s550 => 550.0.h;

  static double get s557 => 557.0.h;

  static double get s570 => 570.0.h;

  static double get s590 => 590.0.h;
}

class AppSizeW {
  static double get s0 => 0.0.w;

  static double get s1 => 1.0.w;

  static double get s1_2 => 1.2.w;

  static double get s1_3 => 1.3.w;

  static double get s1_4 => 1.4.w;

  static double get s1_5 => 1.5.w;

  static double get s1_7 => 1.7.w;

  static double get s1_8 => 1.8.w;

  static double get s1_95 => 1.95.w;

  static double get s2 => 2.0.w;

  static double get s3 => 3.0.w;

  static double get s4 => 4.0.w;

  static double get s4_25 => 4.25.w;

  static double get s5 => 5.0.w;

  static double get s6 => 6.0.w;

  static double get s7 => 7.0.w;

  static double get s8 => 8.0.w;

  static double get s8_5 => 8.5.w;

  static double get s9 => 9.0.w;

  static double get s9_5 => 9.5.w;

  static double get s10 => 10.0.w;

  static double get s10_5 => 10.5.w;

  static double get s11 => 11.0.w;

  static double get s12 => 12.0.w;

  static double get s13 => 13.0.w;

  static double get s13_5 => 13.5.w;

  static double get s14 => 14.0.w;

  static double get s15 => 15.0.w;

  static double get s16 => 16.0.w;

  static double get s16_8 => 16.8.w;

  static double get s17 => 17.0.w;

  static double get s18 => 18.0.w;

  static double get s19 => 19.0.w;

  static double get s20 => 20.0.w;

  static double get s21 => 21.0.w;

  static double get s22 => 22.0.w;

  static double get s23 => 23.0.w;

  static double get s24 => 24.0.w;

  static double get s25 => 25.0.w;

  static double get s26 => 26.0.w;

  static double get s27 => 27.0.w;

  static double get s28 => 28.0.w;

  static double get s28_5 => 28.5.w;

  static double get s29 => 29.w;

  static double get s30 => 30.0.w;

  static double get s31 => 31.0.w;

  static double get s32 => 32.0.w;

  static double get s34 => 34.0.w;

  static double get s35 => 35.0.w;

  static double get s37 => 37.0.w;

  static double get s39 => 39.0.w;

  static double get s40 => 40.0.w;

  static double get s42 => 42.0.w;

  static double get s43 => 43.0.w;

  static double get s44_5 => 44.5.w;

  static double get s47 => 47.0.w;

  static double get s48 => 48.0.w;

  static double get s49 => 49.0.w;

  static double get s50 => 50.0.w;

  static double get s52 => 52.0.w;

  static double get s53 => 53.0.w;

  static double get s54 => 54.0.w;

  static double get s56 => 56.0.w;

  static double get s56_5 => 56.5.w;

  static double get s57 => 57.0.w;

  static double get s60 => 60.0.w;

  static double get s65 => 65.0.w;

  static double get s70 => 70.0.w;

  static double get s72 => 72.0.w;

  static double get s75 => 75.0.w;

  static double get s76 => 76.0.w;

  static double get s80 => 80.0.w;

  static double get s80_5 => 80.5.w;

  static double get s82 => 82.w;

  static double get s90 => 90.0.w;

  static double get s92 => 92.0.w;

  static double get s94 => 94.0.w;

  static double get s95_5 => 95.5.w;

  static double get s95 => 95.0.w;

  static double get s96 => 96.0.w;

  static double get s97 => 97.0.w;

  static double get s99 => 99.0.w;

  static double get s100 => 100.0.w;

  static double get s110 => 110.0.w;

  static double get s103 => 103.0.w;

  static double get s105 => 105.0.w;

  static double get s115 => 115.0.w;

  static double get s112 => 112.0.w;

  static double get s120 => 120.0.w;

  static double get s121 => 121.0.w;

  static double get s122 => 122.0.w;

  static double get s125 => 125.0.w;

  static double get s128 => 128.0.w;

  static double get s130 => 130.0.w;

  static double get s131 => 131.0.w;

  static double get s136 => 136.0.w;

  static double get s137 => 137.0.w;

  static double get s140 => 140.0.w;

  static double get s142 => 142.0.w;

  static double get s145 => 145.0.w;

  static double get s150 => 150.0.w;

  static double get s152 => 152.0.w;

  static double get s160 => 160.0.w;

  static double get s164 => 164.0.w;

  static double get s168 => 168.0.w;

  static double get s170 => 170.0.w;

  static double get s175 => 175.w;

  static double get s180 => 180.0.w;

  static double get s184 => 184.0.w;

  static double get s187 => 187.0.w;

  static double get s190 => 190.0.w;

  static double get s197 => 197.0.w;

  static double get s200 => 200.0.w;

  static double get s210 => 210.0.w;

  static double get s217 => 217.0.w;

  static double get s226 => 226.0.w;

  static double get s250 => 250.0.w;

  static double get s260 => 260.0.w;

  static double get s264 => 264.0.w;

  static double get s265 => 265.0.w;

  static double get s276 => 276.0.w;

  static double get s280 => 280.0.w;

  static double get s290 => 290.0.w;

  static double get s300 => 300.0.w;

  static double get s310 => 310.0.w;

  static double get s312 => 312.0.w;

  static double get s330 => 330.0.w;

  static double get s335 => 335.0.w;

  static double get s346 => 346.0.w;

  static double get s340 => 340.0.w;

  static double get s358 => 358.0.w;

  static double get s360 => 360.0.w;

  static double get s370 => 360.0.w;

  static double get s380 => 380.0.w;

  static double get s387 => 387.0.w;

  static double get s400 => 400.0.w;

  static double get s424 => 424.0.w;

  static double get s460 => 460.0.w;

  static double get s475 => 475.0.w;

  static double get s510 => 510.0.w;

  static double get s550 => 550.0.w;

  static double get s575 => 575.0.w;

  static double get s600 => 600.0.w;

  static double get s650 => 650.0.w;

  static double get s700 => 700.0.w;

  static double get s800 => 800.0.w;
}

class AppSizeR {
  static double get s0 => 0.0.r;

  static double get s1 => 1.0.r;

  static double get s1_4 => 1.4.r;

  static double get s1_5 => 1.5.r;

  static double get s1_7 => 1.7.r;

  static double get s1_8 => 1.8.r;

  static double get s1_95 => 1.95.r;

  static double get s2 => 2.0.r;

  static double get s3 => 3.0.r;

  static double get s4 => 4.0.r;

  static double get s4_25 => 4.25.r;

  static double get s5 => 5.0.r;

  static double get s6 => 6.0.r;

  static double get s7 => 7.0.r;

  static double get s8 => 8.0.r;

  static double get s9 => 9.0.r;

  static double get s10 => 10.0.r;

  static double get s11 => 11.0.r;

  static double get s12 => 12.0.r;

  static double get s13 => 13.0.r;

  static double get s14 => 14.0.r;

  static double get s15 => 15.0.r;

  static double get s16 => 16.0.r;

  static double get s17 => 17.0.r;

  static double get s18 => 18.0.r;

  static double get s19 => 19.0.r;

  static double get s20 => 20.0.r;

  static double get s21 => 21.0.r;

  static double get s22 => 22.0.r;

  static double get s24 => 24.0.r;

  static double get s25 => 25.0.r;

  static double get s26 => 26.0.r;

  static double get s28 => 28.0.r;

  static double get s28_5 => 28.5.r;

  static double get s29 => 29.0.r;

  static double get s30 => 30.0.r;

  static double get s32 => 32.0.r;

  static double get s33 => 33.0.r;

  static double get s36 => 36.0.r;

  static double get s38 => 38.0.r;

  static double get s39 => 39.0.r;

  static double get s40 => 40.0.r;

  static double get s44 => 44.0.r;

  static double get s45 => 45.0.r;

  static double get s50 => 50.0.r;

  static double get s52 => 52.0.r;

  static double get s54 => 54.0.r;

  static double get s56 => 54.0.r;

  static double get s60 => 60.0.r;

  static double get s65 => 65.0.r;

  static double get s72 => 72.0.r;

  static double get s75 => 75.0.r;

  static double get s76 => 76.0.r;

  static double get s80 => 80.0.r;

  static double get s90 => 90.0.r;

  static double get s100 => 100.0.r;

  static double get s103 => 103.0.r;

  static double get s105 => 105.0.r;

  static double get s115 => 115.0.r;

  static double get s120 => 120.0.r;

  static double get s130 => 130.0.r;

  static double get s140 => 140.0.r;

  static double get s150 => 150.0.r;

  static double get s160 => 160.0.r;

  static double get s170 => 170.0.r;

  static double get s180 => 180.0.r;

  static double get s190 => 190.0.r;

  static double get s250 => 250.0.r;
}

class AppSizeSp {
  static double get s0 => 0.0.sp;

  static double get s1 => 1.0.sp;

  static double get s1_4 => 1.4.sp;

  static double get s1_5 => 1.5.sp;

  static double get s1_7 => 1.7.sp;

  static double get s1_8 => 1.8.sp;

  static double get s1_95 => 1.95.sp;

  static double get s2 => 2.0.sp;

  static double get s3 => 3.0.sp;

  static double get s4 => 4.0.sp;

  static double get s4_25 => 4.25.sp;

  static double get s5 => 5.0.sp;

  static double get s6 => 6.0.sp;

  static double get s7 => 7.0.sp;

  static double get s8 => 8.0.sp;

  static double get s9 => 9.0.sp;

  static double get s9_5 => 9.5.sp;

  static double get s10 => 10.0.sp;

  static double get s11 => 11.0.sp;

  static double get s12 => 12.0.sp;

  static double get s13 => 13.0.sp;

  static double get s14 => 14.0.sp;

  static double get s15 => 15.0.sp;

  static double get s16 => 16.0.sp;

  static double get s17 => 17.0.sp;

  static double get s18 => 18.0.sp;

  static double get s18_5 => 18.5.sp;

  static double get s19 => 19.0.sp;

  static double get s20 => 20.0.sp;

  static double get s21 => 21.0.sp;

  static double get s22 => 22.0.sp;

  static double get s23 => 23.0.sp;

  static double get s24 => 24.0.sp;

  static double get s25 => 25.0.sp;

  static double get s28 => 28.0.sp;

  static double get s28_5 => 28.5.sp;

  static double get s30 => 30.0.sp;

  static double get s32 => 32.0.sp;

  static double get s34 => 34.0.sp;

  static double get s36 => 36.0.sp;

  static double get s38 => 38.0.sp;

  static double get s40 => 40.0.sp;

  static double get s45 => 45.0.sp;

  static double get s46 => 46.0.sp;

  static double get s50 => 50.0.sp;

  static double get s54 => 54.0.sp;

  static double get s56 => 54.0.sp;

  static double get s60 => 60.0.sp;

  static double get s65 => 65.0.sp;

  static double get s72 => 72.0.sp;

  static double get s75 => 75.0.sp;

  static double get s80 => 80.0.sp;

  static double get s90 => 90.0.sp;

  static double get s100 => 100.0.sp;

  static double get s103 => 103.0.sp;

  static double get s105 => 105.0.sp;

  static double get s115 => 115.0.sp;

  static double get s120 => 120.0.sp;

  static double get s130 => 130.0.sp;

  static double get s140 => 140.0.sp;

  static double get s150 => 150.0.sp;

  static double get s160 => 160.0.sp;

  static double get s170 => 170.0.sp;

  static double get s180 => 180.0.sp;

  static double get s190 => 190.0.sp;

  static double get s250 => 250.0.sp;
}

class DurationConstant {
  static const int d1 = 1;
  static const int d2 = 2;
  static const int d3 = 3;
  static const int d5 = 5;
  static const Duration d300 = Duration(milliseconds: 300);
  static const Duration d500 = Duration(milliseconds: 500);
}
