import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../extensions/extensions.dart';
import 'color_manager.dart';

class FontConstants {
  static const String almaraiFontFamily = "almarai";
}

enum ThemeDataType { light, dark }

get lightTheme => ThemeData(
    brightness: Brightness.light,
    primaryColor: ColorManager.primaryColor,
    disabledColor: ColorManager.darkGrey,
    primarySwatch: ColorManager.primarySwatch,
    cardColor: ColorManager.cardColor,
    scaffoldBackgroundColor: ColorManager.scaffoldBackgroundColor,
    textTheme: TextTheme(
      headlineLarge: TextStyle(
        fontFamily: FontConstants.almaraiFontFamily,
        fontSize: 25.sp,
        fontWeight: FontWeight.w700,
        color: ColorManager.darkGrey,
      ),
      headlineMedium: TextStyle(
        fontFamily: FontConstants.almaraiFontFamily,
        fontSize: 18.sp,
        fontWeight: FontWeight.w700,
        color: ColorManager.darkGrey,
      ),
      headlineSmall: TextStyle(
        fontFamily: FontConstants.almaraiFontFamily,
        fontSize: 16.sp,
        fontWeight: FontWeight.w700,
        color: ColorManager.lightBlack,
      ),
      bodyLarge: TextStyle(
        fontFamily: FontConstants.almaraiFontFamily,
        fontSize: 18.sp,
        fontWeight: FontWeight.w700,
        color: ColorManager.lightBlack,
      ),
      bodyMedium: TextStyle(
        fontFamily: FontConstants.almaraiFontFamily,
        fontSize: 16.sp,
        fontWeight: FontWeight.w400,
        color: ColorManager.lightBlack,
      ),
      bodySmall: TextStyle(
        fontFamily: FontConstants.almaraiFontFamily,
        fontSize: 14.sp,
        fontWeight: FontWeight.w400,
        color: ColorManager.darkGrey,
      ),
      labelLarge: TextStyle(
        fontFamily: FontConstants.almaraiFontFamily,
        fontSize: 18.sp,
        fontWeight: FontWeight.w400,
        color: ColorManager.black,
      ),
      labelMedium: TextStyle(
        fontFamily: FontConstants.almaraiFontFamily,
        fontSize: 14.sp,
        fontWeight: FontWeight.w400,
        color: ColorManager.black,
      ),
      labelSmall: TextStyle(
        fontFamily: FontConstants.almaraiFontFamily,
        fontSize: 10.sp,
        fontWeight: FontWeight.w400,
        color: ColorManager.black,
      ),
    ),
    floatingActionButtonTheme: const FloatingActionButtonThemeData(
        foregroundColor: ColorManager.primaryColor),
    primaryTextTheme: TextTheme(
      headlineLarge: TextStyle(
        fontFamily: FontConstants.almaraiFontFamily,
        fontSize: 25.sp,
        fontWeight: FontWeight.w700,
        color: ColorManager.primaryColor,
      ),
      headlineMedium: TextStyle(
        fontFamily: FontConstants.almaraiFontFamily,
        fontSize: 18.sp,
        fontWeight: FontWeight.w700,
        color: ColorManager.primaryColor,
      ),
      headlineSmall: TextStyle(
        fontFamily: FontConstants.almaraiFontFamily,
        fontSize: 16.sp,
        fontWeight: FontWeight.w700,
        color: ColorManager.primaryColor,
      ),
      bodyLarge: TextStyle(
        fontFamily: FontConstants.almaraiFontFamily,
        fontSize: 18.sp,
        fontWeight: FontWeight.w700,
        color: ColorManager.primaryColor,
      ),
      bodyMedium: TextStyle(
        fontFamily: FontConstants.almaraiFontFamily,
        fontSize: 16.sp,
        fontWeight: FontWeight.w400,
        color: ColorManager.primaryColor,
      ),
      bodySmall: TextStyle(
        fontFamily: FontConstants.almaraiFontFamily,
        fontSize: 14.sp,
        fontWeight: FontWeight.w400,
        color: ColorManager.primaryColor,
      ),
      labelLarge: TextStyle(
        fontFamily: FontConstants.almaraiFontFamily,
        fontSize: 18.sp,
        fontWeight: FontWeight.w400,
        color: ColorManager.primaryColor,
      ),
      labelMedium: TextStyle(
        fontFamily: FontConstants.almaraiFontFamily,
        fontSize: 14.sp,
        fontWeight: FontWeight.w400,
        color: ColorManager.primaryColor,
      ),
      labelSmall: TextStyle(
        fontFamily: FontConstants.almaraiFontFamily,
        fontSize: 10.sp,
        fontWeight: FontWeight.w400,
        color: ColorManager.primaryColor,
      ),
    ),
    dialogTheme: DialogTheme(
      backgroundColor: ColorManager.surface,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(9.r),
        side: const BorderSide(
          color: ColorManager.primaryColor,
          width: 1,
          style: BorderStyle.solid,
        ),
      ),
    ),
    dataTableTheme: DataTableThemeData(
      headingRowColor: MaterialStateColor.resolveWith(
        (states) => ColorManager.primaryColorSoft,
      ),
      dataRowColor: MaterialStateColor.resolveWith(
        (states) {
          if (states.contains(MaterialState.selected)) {
            return ColorManager.primarySwatch.shade50;
          } else if (states.contains(MaterialState.hovered)) {
            return ColorManager.primarySwatch.shade50;
          }

          return ColorManager.surface;
        },
      ),
      headingTextStyle: TextStyle(
        fontFamily: FontConstants.almaraiFontFamily,
        fontSize: 15.sp,
        fontWeight: FontWeight.w400,
        color: ColorManager.black,
      ),
      dataTextStyle: TextStyle(
        fontFamily: FontConstants.almaraiFontFamily,
        fontSize: 14.sp,
        fontWeight: FontWeight.w400,
        color: ColorManager.black,
      ),
      dividerThickness: 1,
    ),
    colorScheme: const ColorScheme.light(
      primary: ColorManager.primaryColor,
      onPrimary: ColorManager.surface,
      secondary: ColorManager.primaryColorLight,
      primaryContainer: ColorManager.primaryContainer,
      onPrimaryContainer: ColorManager.surface,
      shadow: ColorManager.shadow,
      surface: ColorManager.surface,
      error: ColorManager.error,
      surfaceTint: Colors.transparent,
      brightness: Brightness.light,
    ),
    inputDecorationTheme: InputDecorationTheme(
      hintStyle: TextStyle(
        fontFamily: FontConstants.almaraiFontFamily,
        fontSize: 16.sp,
        fontWeight: FontWeight.w200,
        color: ColorManager.darkGrey,
      ),
      contentPadding: EdgeInsets.symmetric(horizontal: 8.w, vertical: 3.h),
      filled: true,
      errorStyle: TextStyle(
        color: ColorManager.error,
        fontFamily: FontConstants.almaraiFontFamily,
        fontSize: 16.sp,
        fontWeight: FontWeight.w400,
      ),
      fillColor: ColorManager.surface,
      hoverColor: ColorManager.grey,
      focusColor: ColorManager.grey,
      labelStyle: TextStyle(
        fontFamily: FontConstants.almaraiFontFamily,
        fontSize: 16.sp,
        fontWeight: FontWeight.w400,
        color: ColorManager.darkGrey,
      ),
      focusedBorder: OutlineInputBorder(
        borderSide: const BorderSide(
          color: ColorManager.darkGrey,
          width: 1,
          style: BorderStyle.solid,
        ),
        borderRadius: BorderRadius.circular(9.r),
      ),
      errorBorder: OutlineInputBorder(
        borderSide: const BorderSide(
          color: ColorManager.error,
          width: 1,
          style: BorderStyle.solid,
        ),
        borderRadius: BorderRadius.circular(9.r),
      ),
      border: OutlineInputBorder(
        borderSide: BorderSide.none,
        borderRadius: BorderRadius.circular(9.r),
      ),
      disabledBorder: OutlineInputBorder(
        borderSide: BorderSide.none,
        borderRadius: BorderRadius.circular(9.r),
      ),
      enabledBorder: OutlineInputBorder(
        borderSide: BorderSide.none,
        borderRadius: BorderRadius.circular(9.r),
      ),
    ),
    outlinedButtonTheme: OutlinedButtonThemeData(
      style: ButtonStyle(
        shape: MaterialStateProperty.all(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(9.r),
          ),
        ),
        side: MaterialStateProperty.resolveWith<BorderSide>(
          (Set<MaterialState> states) {
            if (states.contains(MaterialState.disabled)) {
              return const BorderSide(
                color: ColorManager.softGrey,
                width: 1,
                style: BorderStyle.solid,
              );
            }
            return const BorderSide(
              color: ColorManager.primaryColor,
              width: 1,
              style: BorderStyle.solid,
            );
          },
        ),
        backgroundColor: MaterialStateProperty.resolveWith<Color>(
          (Set<MaterialState> states) {
            if (states.contains(MaterialState.disabled)) {
              return Colors.transparent;
            } else if (states.contains(MaterialState.hovered)) {
              return ColorManager.primarySwatch.shade100;
            } else if (states.contains(MaterialState.pressed)) {
              return ColorManager.primarySwatch.shade300;
            }
            return Colors.transparent;
          },
        ),
        foregroundColor: MaterialStateProperty.resolveWith<Color>(
          (Set<MaterialState> states) {
            if (states.contains(MaterialState.disabled)) {
              return ColorManager.softGrey;
            } else if (states.contains(MaterialState.pressed)) {
              return Colors.white;
            }
            return ColorManager.primaryColor;
          },
        ),
        overlayColor: MaterialStateProperty.resolveWith<Color>(
          (Set<MaterialState> states) {
            if (states.contains(MaterialState.hovered)) {
              return ColorManager.primarySwatch.shade100;
            } else if (states.contains(MaterialState.pressed)) {
              return ColorManager.primarySwatch.shade300;
            }
            return ColorManager.surface;
          },
        ),
        padding: MaterialStateProperty.resolveWith<EdgeInsetsGeometry>(
          (Set<MaterialState> states) {
            if (states.contains(MaterialState.disabled)) {
              return EdgeInsets.symmetric(
                horizontal: 20.w,
                vertical: 10.h,
              );
            } else if (states.contains(MaterialState.hovered)) {
              return EdgeInsets.symmetric(
                horizontal: 23.w,
                vertical: 10.h,
              );
            }
            return EdgeInsets.symmetric(
              horizontal: 20.w,
              vertical: 10.h,
            );
          },
        ),
        textStyle: MaterialStateProperty.resolveWith<TextStyle>(
          (Set<MaterialState> states) {
            if (states.contains(MaterialState.disabled)) {
              return TextStyle(
                fontFamily: FontConstants.almaraiFontFamily,
                fontSize: 15.sp,
                fontWeight: FontWeight.w400,
                color: ColorManager.softGrey,
              );
            } else if (states.contains(MaterialState.hovered)) {
              return TextStyle(
                fontFamily: FontConstants.almaraiFontFamily,
                fontSize: 16.sp,
                fontWeight: FontWeight.w400,
                color: ColorManager.primaryColor,
              );
            } else if (states.contains(MaterialState.pressed)) {
              return TextStyle(
                fontFamily: FontConstants.almaraiFontFamily,
                fontSize: 16.sp,
                fontWeight: FontWeight.w400,
                color: ColorManager.primaryColor,
              );
            }
            return TextStyle(
              fontFamily: FontConstants.almaraiFontFamily,
              fontSize: 15.sp,
              fontWeight: FontWeight.w400,
              color: ColorManager.primaryColor,
            );
          },
        ),
      ),
    ),
    elevatedButtonTheme: ElevatedButtonThemeData(
        style: ButtonStyle(
      backgroundColor: MaterialStateProperty.resolveWith<Color>(
        (Set<MaterialState> states) {
          if (states.contains(MaterialState.disabled)) {
            return ColorManager.grey;
          } else if (states.contains(MaterialState.hovered)) {
            return ColorManager.primarySwatch.shade700;
          } else if (states.contains(MaterialState.pressed)) {
            return ColorManager.primaryColorDark;
          }
          return ColorManager.primaryColor;
        },
      ),
      overlayColor: MaterialStateProperty.resolveWith<Color>(
        (Set<MaterialState> states) {
          if (states.contains(MaterialState.hovered)) {
            return ColorManager.primarySwatch.shade700;
          } else if (states.contains(MaterialState.pressed)) {
            return ColorManager.primaryColorDark;
          }
          return ColorManager.surface;
        },
      ),
      foregroundColor: MaterialStateProperty.resolveWith<Color>(
        (Set<MaterialState> states) {
          if (states.contains(MaterialState.disabled)) {
            return ColorManager.softGrey;
          } else if (states.contains(MaterialState.hovered)) {
            return ColorManager.surface;
          } else if (states.contains(MaterialState.pressed)) {
            return ColorManager.surface;
          }
          return ColorManager.surface;
        },
      ),
      shape: MaterialStateProperty.all(
        RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(9.r),
        ),
      ),
      textStyle: MaterialStateProperty.resolveWith<TextStyle>(
        (Set<MaterialState> states) {
          if (states.contains(MaterialState.disabled)) {
            return TextStyle(
              fontFamily: FontConstants.almaraiFontFamily,
              fontSize: 15.sp,
              fontWeight: FontWeight.w400,
              color: ColorManager.surface,
            );
          } else if (states.contains(MaterialState.hovered)) {
            return TextStyle(
              fontFamily: FontConstants.almaraiFontFamily,
              fontSize: 16.sp,
              fontWeight: FontWeight.w400,
              color: ColorManager.surface,
            );
          } else if (states.contains(MaterialState.pressed)) {
            return TextStyle(
              fontFamily: FontConstants.almaraiFontFamily,
              fontSize: 16.sp,
              fontWeight: FontWeight.w400,
              color: ColorManager.surface,
            );
          }
          return TextStyle(
            fontFamily: FontConstants.almaraiFontFamily,
            fontSize: 15.sp,
            fontWeight: FontWeight.w400,
            color: ColorManager.surface,
          );
        },
      ),
    )),
    textButtonTheme: TextButtonThemeData(
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.resolveWith<Color>(
          (Set<MaterialState> states) {
            if (states.contains(MaterialState.disabled)) {
              return Colors.transparent;
            } else if (states.contains(MaterialState.hovered)) {
              return ColorManager.primarySwatch.shade100;
            } else if (states.contains(MaterialState.pressed)) {
              return ColorManager.primarySwatch.shade300;
            }
            return Colors.transparent;
          },
        ),
        overlayColor: MaterialStateProperty.resolveWith<Color>(
          (Set<MaterialState> states) {
            if (states.contains(MaterialState.hovered)) {
              return ColorManager.primarySwatch.shade100;
            } else if (states.contains(MaterialState.pressed)) {
              return ColorManager.primarySwatch.shade300;
            }
            return ColorManager.surface;
          },
        ),
        foregroundColor: MaterialStateProperty.resolveWith<Color>(
          (Set<MaterialState> states) {
            if (states.contains(MaterialState.disabled)) {
              return ColorManager.softGrey;
            } else if (states.contains(MaterialState.hovered)) {
              return ColorManager.primaryColor;
            } else if (states.contains(MaterialState.pressed)) {
              return ColorManager.primaryColor;
            }
            return ColorManager.primaryColor;
          },
        ),
        shape: MaterialStateProperty.all(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(9.r),
          ),
        ),
        textStyle: MaterialStateProperty.resolveWith<TextStyle>(
          (Set<MaterialState> states) {
            if (states.contains(MaterialState.disabled)) {
              return TextStyle(
                fontFamily: FontConstants.almaraiFontFamily,
                fontSize: 15.sp,
                fontWeight: FontWeight.w400,
                color: ColorManager.softGrey,
              );
            } else if (states.contains(MaterialState.hovered)) {
              return TextStyle(
                fontFamily: FontConstants.almaraiFontFamily,
                fontSize: 16.sp,
                fontWeight: FontWeight.w400,
                color: ColorManager.primaryColor,
              );
            } else if (states.contains(MaterialState.pressed)) {
              return TextStyle(
                fontFamily: FontConstants.almaraiFontFamily,
                fontSize: 16.sp,
                fontWeight: FontWeight.w400,
                color: ColorManager.primaryColor,
              );
            } else if (states.contains(MaterialState.focused)) {
              return TextStyle(
                fontFamily: FontConstants.almaraiFontFamily,
                fontSize: 16.sp,
                fontWeight: FontWeight.w400,
                color: ColorManager.primaryColor,
              );
            }
            return TextStyle(
              fontFamily: FontConstants.almaraiFontFamily,
              fontSize: 15.sp,
              fontWeight: FontWeight.w400,
              color: ColorManager.primaryColor,
            );
          },
        ),
      ),
    ),
    iconButtonTheme: IconButtonThemeData(
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.resolveWith<Color>(
          (Set<MaterialState> states) {
            if (states.contains(MaterialState.disabled)) {
              return Colors.transparent;
            } else if (states.contains(MaterialState.hovered)) {
              return ColorManager.primarySwatch.shade100;
            } else if (states.contains(MaterialState.pressed)) {
              return ColorManager.primarySwatch.shade300;
            }
            return Colors.transparent;
          },
        ),
        overlayColor: MaterialStateProperty.resolveWith<Color>(
          (Set<MaterialState> states) {
            if (states.contains(MaterialState.hovered)) {
              return ColorManager.primarySwatch.shade100;
            } else if (states.contains(MaterialState.pressed)) {
              return ColorManager.primarySwatch.shade300;
            }
            return ColorManager.surface;
          },
        ),
        foregroundColor: MaterialStateProperty.resolveWith<Color>(
          (Set<MaterialState> states) {
            if (states.contains(MaterialState.disabled)) {
              return ColorManager.softGrey;
            } else if (states.contains(MaterialState.hovered)) {
              return ColorManager.primaryColor;
            } else if (states.contains(MaterialState.pressed)) {
              return ColorManager.primaryColor;
            }
            return ColorManager.primaryColor;
          },
        ),
        shape: MaterialStateProperty.all(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(9.r),
          ),
        ),
        textStyle: MaterialStateProperty.resolveWith<TextStyle>(
          (Set<MaterialState> states) {
            if (states.contains(MaterialState.disabled)) {
              return TextStyle(
                fontFamily: FontConstants.almaraiFontFamily,
                fontSize: 15.sp,
                fontWeight: FontWeight.w400,
                color: ColorManager.softGrey,
              );
            } else if (states.contains(MaterialState.hovered)) {
              return TextStyle(
                fontFamily: FontConstants.almaraiFontFamily,
                fontSize: 16.sp,
                fontWeight: FontWeight.w400,
                color: ColorManager.primaryColor,
              );
            } else if (states.contains(MaterialState.pressed)) {
              return TextStyle(
                fontFamily: FontConstants.almaraiFontFamily,
                fontSize: 16.sp,
                fontWeight: FontWeight.w400,
                color: ColorManager.primaryColor,
              );
            } else if (states.contains(MaterialState.focused)) {
              return TextStyle(
                fontFamily: FontConstants.almaraiFontFamily,
                fontSize: 16.sp,
                fontWeight: FontWeight.w400,
                color: ColorManager.primaryColor,
              );
            }
            return TextStyle(
              fontFamily: FontConstants.almaraiFontFamily,
              fontSize: 15.sp,
              fontWeight: FontWeight.w400,
              color: ColorManager.primaryColor,
            );
          },
        ),
      ),
    ));

get darkTheme => ThemeData(
      brightness: Brightness.dark,
    );

extension BorderThemeExtinssion on ThemeData {
  BorderSide get defaultBorderSide => (isDarkTheme)
      ? const BorderSide(
          color: ColorManager.primaryColor,
          width: 1,
          style: BorderStyle.solid,
        )
      : const BorderSide(
          color: ColorManager.primaryColor, width: 1, style: BorderStyle.solid);

  InputDecorationTheme get secondaryInputBorderTheme => InputDecorationTheme(
        filled: true,
        errorStyle: TextStyle(
          color: ColorManager.error,
          fontFamily: FontConstants.almaraiFontFamily,
          fontSize: 16.sp,
          fontWeight: FontWeight.w400,
        ),
        fillColor: ColorManager.surface,
        hoverColor: ColorManager.grey,
        focusColor: ColorManager.grey,
        labelStyle: TextStyle(
          fontFamily: FontConstants.almaraiFontFamily,
          fontSize: 16.sp,
          fontWeight: FontWeight.w400,
          color: ColorManager.darkGrey,
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: const BorderSide(
            color: ColorManager.greyLight,
            width: 1,
            style: BorderStyle.solid,
          ),
          borderRadius: BorderRadius.circular(9.r),
        ),
        errorBorder: OutlineInputBorder(
          borderSide: const BorderSide(
            color: ColorManager.error,
            width: 1,
            style: BorderStyle.solid,
          ),
          borderRadius: BorderRadius.circular(9.r),
        ),
        border: OutlineInputBorder(
          borderSide: BorderSide.none,
          borderRadius: BorderRadius.circular(9.r),
        ),
        disabledBorder: OutlineInputBorder(
          borderSide: BorderSide.none,
          borderRadius: BorderRadius.circular(9.r),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide.none,
          borderRadius: BorderRadius.circular(9.r),
        ),
      );

  BorderSide get defaultBorderSideLight => const BorderSide(
        color: ColorManager.primaryColorLight,
        width: 1,
        style: BorderStyle.solid,
      );

  BorderSide get defaultBorderSideError => const BorderSide(
        color: ColorManager.error,
        width: 1,
        style: BorderStyle.solid,
      );

  BorderSide get defaultBorderSideShadow => const BorderSide(
        color: ColorManager.shadow,
        width: 1,
        style: BorderStyle.solid,
      );
}

extension ButtonThemeExtinssion on ThemeData {
  ButtonStyle get secondaryElevatedButtonTheme => ButtonStyle(
        backgroundColor: MaterialStateProperty.resolveWith<Color>(
          (Set<MaterialState> states) {
            if (states.contains(MaterialState.disabled)) {
              return ColorManager.grey;
            } else if (states.contains(MaterialState.hovered)) {
              return ColorManager.secondaryGreyLight;
            } else if (states.contains(MaterialState.pressed)) {
              return ColorManager.secondaryGreyDark;
            }
            return ColorManager.secondaryGrey;
          },
        ),
        overlayColor: MaterialStateProperty.resolveWith<Color>(
            (Set<MaterialState> states) {
          if (states.contains(MaterialState.hovered)) {
            return ColorManager.secondaryGreyLight;
          } else if (states.contains(MaterialState.pressed)) {
            return ColorManager.secondaryGreyDark;
          }
          return ColorManager.surface;
        }),
        foregroundColor: MaterialStateProperty.resolveWith<Color>(
          (Set<MaterialState> states) {
            if (states.contains(MaterialState.disabled)) {
              return ColorManager.softGrey;
            } else if (states.contains(MaterialState.hovered)) {
              return ColorManager.surface;
            } else if (states.contains(MaterialState.pressed)) {
              return ColorManager.surface;
            }
            return ColorManager.surface;
          },
        ),
        shape: MaterialStateProperty.all(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(9.r),
          ),
        ),
        textStyle: MaterialStateProperty.resolveWith<TextStyle>(
          (Set<MaterialState> states) {
            if (states.contains(MaterialState.disabled)) {
              return TextStyle(
                fontFamily: FontConstants.almaraiFontFamily,
                fontSize: 15.sp,
                fontWeight: FontWeight.w400,
                color: ColorManager.surface,
              );
            } else if (states.contains(MaterialState.hovered)) {
              return TextStyle(
                fontFamily: FontConstants.almaraiFontFamily,
                fontSize: 16.sp,
                fontWeight: FontWeight.w400,
                color: ColorManager.surface,
              );
            } else if (states.contains(MaterialState.pressed)) {
              return TextStyle(
                fontFamily: FontConstants.almaraiFontFamily,
                fontSize: 16.sp,
                fontWeight: FontWeight.w400,
                color: ColorManager.surface,
              );
            }
            return TextStyle(
              fontFamily: FontConstants.almaraiFontFamily,
              fontSize: 15.sp,
              fontWeight: FontWeight.w400,
              color: ColorManager.surface,
            );
          },
        ),
        padding: MaterialStateProperty.resolveWith<EdgeInsetsGeometry>(
          (Set<MaterialState> states) {
            if (states.contains(MaterialState.disabled)) {
              return EdgeInsets.symmetric(
                horizontal: 20.w,
                vertical: 10.h,
              );
            } else if (states.contains(MaterialState.hovered)) {
              return EdgeInsets.symmetric(
                horizontal: 23.w,
                vertical: 10.h,
              );
            }
            return EdgeInsets.symmetric(
              horizontal: 20.w,
              vertical: 10.h,
            );
          },
        ),
      );

  ButtonStyle get errorElevatedButtonTheme => ButtonStyle(
        backgroundColor: MaterialStateProperty.resolveWith<Color>(
          (Set<MaterialState> states) {
            if (states.contains(MaterialState.disabled)) {
              return ColorManager.grey;
            } else if (states.contains(MaterialState.hovered)) {
              return ColorManager.errorLight;
            } else if (states.contains(MaterialState.pressed)) {
              return ColorManager.error;
            }
            return ColorManager.error;
          },
        ),
        overlayColor: MaterialStateProperty.resolveWith<Color>(
          (Set<MaterialState> states) {
            if (states.contains(MaterialState.hovered)) {
              return ColorManager.errorLight;
            } else if (states.contains(MaterialState.pressed)) {
              return ColorManager.error;
            }
            return ColorManager.surface;
          },
        ),
        foregroundColor: MaterialStateProperty.resolveWith<Color>(
          (Set<MaterialState> states) {
            if (states.contains(MaterialState.disabled)) {
              return ColorManager.softGrey;
            } else if (states.contains(MaterialState.hovered)) {
              return ColorManager.surface;
            } else if (states.contains(MaterialState.pressed)) {
              return ColorManager.surface;
            }
            return ColorManager.surface;
          },
        ),
        shape: MaterialStateProperty.all(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(9.r),
          ),
        ),
        textStyle: MaterialStateProperty.resolveWith<TextStyle>(
          (Set<MaterialState> states) {
            if (states.contains(MaterialState.disabled)) {
              return TextStyle(
                fontFamily: FontConstants.almaraiFontFamily,
                fontSize: 15.sp,
                fontWeight: FontWeight.w400,
                color: ColorManager.surface,
              );
            } else if (states.contains(MaterialState.hovered)) {
              return TextStyle(
                fontFamily: FontConstants.almaraiFontFamily,
                fontSize: 16.sp,
                fontWeight: FontWeight.w400,
                color: ColorManager.surface,
              );
            } else if (states.contains(MaterialState.pressed)) {
              return TextStyle(
                fontFamily: FontConstants.almaraiFontFamily,
                fontSize: 16.sp,
                fontWeight: FontWeight.w400,
                color: ColorManager.surface,
              );
            }
            return TextStyle(
              fontFamily: FontConstants.almaraiFontFamily,
              fontSize: 15.sp,
              fontWeight: FontWeight.w400,
              color: ColorManager.surface,
            );
          },
        ),
        padding: MaterialStateProperty.resolveWith<EdgeInsetsGeometry>(
          (Set<MaterialState> states) {
            if (states.contains(MaterialState.disabled)) {
              return EdgeInsets.symmetric(
                horizontal: 20.w,
                vertical: 10.h,
              );
            } else if (states.contains(MaterialState.hovered)) {
              return EdgeInsets.symmetric(
                horizontal: 23.w,
                vertical: 10.h,
              );
            }
            return EdgeInsets.symmetric(
              horizontal: 20.w,
              vertical: 10.h,
            );
          },
        ),
      );

  ButtonStyle get surfaceElevatedButtonTheme => ButtonStyle(
        backgroundColor: MaterialStateProperty.resolveWith<Color>(
          (Set<MaterialState> states) {
            if (states.contains(MaterialState.disabled)) {
              return ColorManager.grey;
            } else if (states.contains(MaterialState.hovered)) {
              return ColorManager.whiteGrey;
            } else if (states.contains(MaterialState.pressed)) {
              return ColorManager.surface;
            }
            return ColorManager.surface;
          },
        ),
        overlayColor: MaterialStateProperty.resolveWith<Color>(
          (Set<MaterialState> states) {
            if (states.contains(MaterialState.hovered)) {
              return ColorManager.whiteGrey;
            } else if (states.contains(MaterialState.pressed)) {
              return ColorManager.surface;
            }
            return ColorManager.surface;
          },
        ),
        foregroundColor: MaterialStateProperty.resolveWith<Color>(
          (Set<MaterialState> states) {
            if (states.contains(MaterialState.disabled)) {
              return ColorManager.softGrey;
            } else if (states.contains(MaterialState.hovered)) {
              return ColorManager.darkGrey;
            } else if (states.contains(MaterialState.pressed)) {
              return ColorManager.darkGrey;
            }
            return ColorManager.darkGrey;
          },
        ),
        shape: MaterialStateProperty.all(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(9.r),
          ),
        ),
        textStyle: MaterialStateProperty.resolveWith<TextStyle>(
          (Set<MaterialState> states) {
            if (states.contains(MaterialState.disabled)) {
              return TextStyle(
                fontFamily: FontConstants.almaraiFontFamily,
                fontSize: 15.sp,
                fontWeight: FontWeight.w400,
                color: ColorManager.greyLight,
              );
            } else if (states.contains(MaterialState.hovered)) {
              return TextStyle(
                fontFamily: FontConstants.almaraiFontFamily,
                fontSize: 16.sp,
                fontWeight: FontWeight.w400,
                color: ColorManager.secondaryGrey,
              );
            } else if (states.contains(MaterialState.pressed)) {
              return TextStyle(
                fontFamily: FontConstants.almaraiFontFamily,
                fontSize: 16.sp,
                fontWeight: FontWeight.w400,
                color: ColorManager.secondaryGrey,
              );
            }
            return TextStyle(
              fontFamily: FontConstants.almaraiFontFamily,
              fontSize: 15.sp,
              fontWeight: FontWeight.w400,
              color: ColorManager.secondaryGrey,
            );
          },
        ),
        padding: MaterialStateProperty.resolveWith<EdgeInsetsGeometry>(
          (Set<MaterialState> states) {
            if (states.contains(MaterialState.disabled)) {
              return EdgeInsets.symmetric(
                horizontal: 20.w,
                vertical: 10.h,
              );
            } else if (states.contains(MaterialState.hovered)) {
              return EdgeInsets.symmetric(
                horizontal: 23.w,
                vertical: 10.h,
              );
            }
            return EdgeInsets.symmetric(
              horizontal: 20.w,
              vertical: 10.h,
            );
          },
        ),
      );

  ButtonStyle get secondaryTextButtonTheme => ButtonStyle(
        backgroundColor: MaterialStateProperty.resolveWith<Color>(
          (Set<MaterialState> states) {
            if (states.contains(MaterialState.disabled)) {
              return Colors.transparent;
            } else if (states.contains(MaterialState.hovered)) {
              return ColorManager.softGrey;
            } else if (states.contains(MaterialState.pressed)) {
              return ColorManager.softGrey;
            }
            return Colors.transparent;
          },
        ),
        overlayColor: MaterialStateProperty.resolveWith<Color>(
          (Set<MaterialState> states) {
            if (states.contains(MaterialState.hovered)) {
              return ColorManager.softGrey;
            } else if (states.contains(MaterialState.pressed)) {
              return ColorManager.softGrey;
            }
            return ColorManager.surface;
          },
        ),
        foregroundColor: MaterialStateProperty.resolveWith<Color>(
          (Set<MaterialState> states) {
            if (states.contains(MaterialState.disabled)) {
              return ColorManager.softGrey;
            } else if (states.contains(MaterialState.hovered)) {
              return ColorManager.secondaryGrey;
            } else if (states.contains(MaterialState.pressed)) {
              return ColorManager.secondaryGrey;
            }
            return ColorManager.secondaryGrey;
          },
        ),
        shape: MaterialStateProperty.all(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(9.r),
          ),
        ),
        textStyle: MaterialStateProperty.resolveWith<TextStyle>(
          (Set<MaterialState> states) {
            if (states.contains(MaterialState.disabled)) {
              return TextStyle(
                fontFamily: FontConstants.almaraiFontFamily,
                fontSize: 15.sp,
                fontWeight: FontWeight.w400,
                color: ColorManager.softGrey,
              );
            } else if (states.contains(MaterialState.hovered)) {
              return TextStyle(
                fontFamily: FontConstants.almaraiFontFamily,
                fontSize: 16.sp,
                fontWeight: FontWeight.w400,
                color: ColorManager.secondaryGrey,
              );
            } else if (states.contains(MaterialState.pressed)) {
              return TextStyle(
                fontFamily: FontConstants.almaraiFontFamily,
                fontSize: 16.sp,
                fontWeight: FontWeight.w400,
                color: ColorManager.secondaryGrey,
              );
            }
            return TextStyle(
              fontFamily: FontConstants.almaraiFontFamily,
              fontSize: 15.sp,
              fontWeight: FontWeight.w400,
              color: ColorManager.secondaryGrey,
            );
          },
        ),
      );

  ButtonStyle get errorTextButtonTheme => ButtonStyle(
        backgroundColor: MaterialStateProperty.resolveWith<Color>(
          (Set<MaterialState> states) {
            if (states.contains(MaterialState.disabled)) {
              return Colors.transparent;
            } else if (states.contains(MaterialState.hovered)) {
              return ColorManager.errorLightOpacity;
            } else if (states.contains(MaterialState.pressed)) {
              return ColorManager.errorLight;
            }
            return Colors.transparent;
          },
        ),
        overlayColor: MaterialStateProperty.resolveWith<Color>(
          (Set<MaterialState> states) {
            if (states.contains(MaterialState.hovered)) {
              return ColorManager.errorLightOpacity;
            } else if (states.contains(MaterialState.pressed)) {
              return ColorManager.errorLight;
            }
            return ColorManager.surface;
          },
        ),
        foregroundColor: MaterialStateProperty.resolveWith<Color>(
          (Set<MaterialState> states) {
            if (states.contains(MaterialState.disabled)) {
              return ColorManager.softGrey;
            } else if (states.contains(MaterialState.hovered)) {
              return ColorManager.error;
            } else if (states.contains(MaterialState.pressed)) {
              return ColorManager.error;
            }
            return ColorManager.error;
          },
        ),
        shape: MaterialStateProperty.all(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(9.r),
          ),
        ),
        textStyle: MaterialStateProperty.resolveWith<TextStyle>(
          (Set<MaterialState> states) {
            if (states.contains(MaterialState.disabled)) {
              return TextStyle(
                fontFamily: FontConstants.almaraiFontFamily,
                fontSize: 15.sp,
                fontWeight: FontWeight.w400,
                color: ColorManager.softGrey,
              );
            } else if (states.contains(MaterialState.hovered)) {
              return TextStyle(
                fontFamily: FontConstants.almaraiFontFamily,
                fontSize: 16.sp,
                fontWeight: FontWeight.w400,
                color: ColorManager.error,
              );
            } else if (states.contains(MaterialState.pressed)) {
              return TextStyle(
                fontFamily: FontConstants.almaraiFontFamily,
                fontSize: 16.sp,
                fontWeight: FontWeight.w400,
                color: ColorManager.error,
              );
            }
            return TextStyle(
              fontFamily: FontConstants.almaraiFontFamily,
              fontSize: 15.sp,
              fontWeight: FontWeight.w400,
              color: ColorManager.error,
            );
          },
        ),
      );

  ButtonStyle get secondaryIconButtonTheme => ButtonStyle(
        backgroundColor: MaterialStateProperty.resolveWith<Color>(
          (Set<MaterialState> states) {
            if (states.contains(MaterialState.disabled)) {
              return Colors.transparent;
            } else if (states.contains(MaterialState.hovered)) {
              return ColorManager.secondaryGreyLight;
            } else if (states.contains(MaterialState.pressed)) {
              return ColorManager.secondaryGreyDark;
            }
            return Colors.transparent;
          },
        ),
        overlayColor: MaterialStateProperty.resolveWith<Color>(
          (Set<MaterialState> states) {
            if (states.contains(MaterialState.hovered)) {
              return ColorManager.secondaryGreyLight;
            } else if (states.contains(MaterialState.pressed)) {
              return ColorManager.secondaryGreyDark;
            }
            return ColorManager.surface;
          },
        ),
        foregroundColor: MaterialStateProperty.resolveWith<Color>(
          (Set<MaterialState> states) {
            if (states.contains(MaterialState.disabled)) {
              return ColorManager.softGrey;
            } else if (states.contains(MaterialState.hovered)) {
              return ColorManager.secondaryGrey;
            } else if (states.contains(MaterialState.pressed)) {
              return ColorManager.secondaryGrey;
            }
            return ColorManager.secondaryGrey;
          },
        ),
        shape: MaterialStateProperty.all(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(9.r),
          ),
        ),
        textStyle: MaterialStateProperty.resolveWith<TextStyle>(
          (Set<MaterialState> states) {
            if (states.contains(MaterialState.disabled)) {
              return TextStyle(
                fontFamily: FontConstants.almaraiFontFamily,
                fontSize: 15.sp,
                fontWeight: FontWeight.w400,
                color: ColorManager.softGrey,
              );
            } else if (states.contains(MaterialState.hovered)) {
              return TextStyle(
                fontFamily: FontConstants.almaraiFontFamily,
                fontSize: 16.sp,
                fontWeight: FontWeight.w400,
                color: ColorManager.secondaryGrey,
              );
            } else if (states.contains(MaterialState.pressed)) {
              return TextStyle(
                fontFamily: FontConstants.almaraiFontFamily,
                fontSize: 16.sp,
                fontWeight: FontWeight.w400,
                color: ColorManager.secondaryGrey,
              );
            }
            return TextStyle(
              fontFamily: FontConstants.almaraiFontFamily,
              fontSize: 15.sp,
              fontWeight: FontWeight.w400,
              color: ColorManager.secondaryGrey,
            );
          },
        ),
      );

  DataTableThemeData get inactiveDataRow => DataTableThemeData(
        dataTextStyle: TextStyle(
          fontFamily: FontConstants.almaraiFontFamily,
          fontSize: 14.sp,
          fontWeight: FontWeight.w400,
          color: ColorManager.softGrey,
        ),
        dividerThickness: 1,
      );

  ButtonStyle get errorIconButtonTheme => ButtonStyle(
        backgroundColor: MaterialStateProperty.resolveWith<Color>(
          (Set<MaterialState> states) {
            if (states.contains(MaterialState.disabled)) {
              return Colors.transparent;
            } else if (states.contains(MaterialState.hovered)) {
              return ColorManager.errorLightOpacity;
            } else if (states.contains(MaterialState.pressed)) {
              return ColorManager.error;
            }
            return Colors.transparent;
          },
        ),
        overlayColor: MaterialStateProperty.resolveWith<Color>(
          (Set<MaterialState> states) {
            if (states.contains(MaterialState.hovered)) {
              return ColorManager.errorLightOpacity;
            } else if (states.contains(MaterialState.pressed)) {
              return ColorManager.error;
            }
            return ColorManager.surface;
          },
        ),
        foregroundColor: MaterialStateProperty.resolveWith<Color>(
          (Set<MaterialState> states) {
            if (states.contains(MaterialState.disabled)) {
              return ColorManager.softGrey;
            } else if (states.contains(MaterialState.hovered)) {
              return ColorManager.error;
            } else if (states.contains(MaterialState.pressed)) {
              return ColorManager.error;
            }
            return ColorManager.error;
          },
        ),
        shape: MaterialStateProperty.all(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(9.r),
          ),
        ),
        textStyle: MaterialStateProperty.resolveWith<TextStyle>(
          (Set<MaterialState> states) {
            if (states.contains(MaterialState.disabled)) {
              return TextStyle(
                fontFamily: FontConstants.almaraiFontFamily,
                fontSize: 15.sp,
                fontWeight: FontWeight.w400,
                color: ColorManager.softGrey,
              );
            } else if (states.contains(MaterialState.hovered)) {
              return TextStyle(
                fontFamily: FontConstants.almaraiFontFamily,
                fontSize: 16.sp,
                fontWeight: FontWeight.w400,
                color: ColorManager.error,
              );
            } else if (states.contains(MaterialState.pressed)) {
              return TextStyle(
                fontFamily: FontConstants.almaraiFontFamily,
                fontSize: 16.sp,
                fontWeight: FontWeight.w400,
                color: ColorManager.error,
              );
            }
            return TextStyle(
              fontFamily: FontConstants.almaraiFontFamily,
              fontSize: 15.sp,
              fontWeight: FontWeight.w400,
              color: ColorManager.error,
            );
          },
        ),
      );
}
