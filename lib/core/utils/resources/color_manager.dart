import 'package:flutter/material.dart';

class ColorManager {
  //fixme refactor this class to  use fromHex method
  static const MaterialColor primarySwatch = MaterialColor(
    0xFF3B9E49,
    <int, Color>{
      50: Color(0xFFDEEFE1),
      100: Color(0xFFB9DCBE),
      200: Color(0xFFA7D3AD),
      300: Color(0xFF95CB9D),
      400: Color(0xFF83C28C),
      500: Color(0xFF71B97B),
      600: Color(0xFF71B97B),
      700: Color(0xFF5FB06A),
      800: Color(0xFF4DA75A),
      900: Color(0xFF3B9E49),
    },
  );
  static const primaryColor = Color(0xFF3B9E49);
  static const primaryColorDark = Color(0xFF287633);
  static const Color primaryColorLight = Color(0xFFAFE3B6);
  static const Color primaryColorSoft = Color(0xFFDEEFE1);
  static const Color primaryColorLightOpacity =
      Color.fromRGBO(59, 158, 73, 0.23);
  static const Color primaryContainer = Color(0xFFEEEEEE);
  static const Color onPrimaryContainer = Color(0xFFFFFFFF);
  static const Color surface = Color(0xFFFFFFFF);
  static const Color cardColor = Color(0xFFFFFFFF);
  static const Color error = Color(0xFFCE3C3C);
  static const Color errorLight = Color(0xFFE94343);
  //#CE3C3C33
  static const Color errorLightOpacity = Color.fromRGBO(206, 60, 60, 0.2);
  static const Color shadow = Color(0xFF000029);
  static const Color black = Color(0xFF000000);
  static const Color lightBlack = Color(0xFF131A31);
  static const Color darkGrey = Color(0xFF6D6D6D);
  static const Color whiteGrey = Color(0xFFF4F4F4);
  static const Color greyLight = Color(0xFFC4C4C4);
  static const Color softGrey = Color(0xFFB6B7B7);
  static const Color grey = Color(0xFFEEEEEE);
  static const Color scaffoldBackgroundColor = Color(0xFFF7F7F7);
  static const Color secondaryGreyLight = Color(0xFF939393);
  static const Color secondaryGrey = Color(0xFF6D6D6D);
  static const Color secondaryGreyDark = Color(0xFF464646);
}

extension tes on String {
  Color toColor() {
    return Color(int.parse(replaceAll('#', '0xFF')));
  }
}

extension Hex16Color on Color {
  String toHex16String() {
    return '#${value.toRadixString(16).padLeft(8, '0').toUpperCase()}';
  }

  static Color fromHex(String hexColorString) {
    hexColorString = hexColorString.replaceAll('#', '');
    if (hexColorString.length == 6) {
      hexColorString = "FF$hexColorString"; // 8 char with opacity 100%
    }
    return Color(int.parse(hexColorString, radix: 16));
  }
}
