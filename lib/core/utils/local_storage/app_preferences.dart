import 'package:shared_preferences/shared_preferences.dart';
import 'package:tm/core/utils/local_storage/task/task_preferences.dart';

import '../../../../core/utils/local_storage/language/language_prefrance.dart';
import '../../../../core/utils/local_storage/theme/theme_prefrance.dart';
import '../../../../core/utils/local_storage/user/user_preferance.dart';

class AppPreferences {
  late final SharedPreferences sharedPreferences;
  late final LanguagePreferences _languagePreferences;
  late final ThemePreferences _themePreferences;
  late final UserPreferences _userPreferences;
  late final TaskPreferences _taskPreferences;

  LanguagePreferences get languagePreferences => _languagePreferences;

  ThemePreferences get themePreferences => _themePreferences;

  UserPreferences get userPreferences => _userPreferences;

  TaskPreferences get taskPreferences => _taskPreferences;

  AppPreferences(this.sharedPreferences) {
    _languagePreferences = LanguagePreferences(sharedPreferences);
    _themePreferences = ThemePreferences(sharedPreferences);
    _userPreferences = UserPreferences(sharedPreferences);
    _taskPreferences = TaskPreferences(sharedPreferences);
  }
}
