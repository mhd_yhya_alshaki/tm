import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:tm/features/auth/entities/user/user.dart';

class UserPreferences {
  late final SharedPreferences sharedPreferences;

  String prefsTokenKey = 'token';
  String prefsUserKey = 'user';
  String prefsUserWarningMessageKey = 'user_warning_message';

  UserPreferences(this.sharedPreferences);

  Future<void> setUserToken(String token) async {
    await sharedPreferences.setString(prefsTokenKey, token);
  }

  String getUserToken() {
    return sharedPreferences.getString(prefsTokenKey) ?? '';
  }

  Future<void> setUserIsSeeMessage() async {
    await sharedPreferences.setBool(prefsUserWarningMessageKey, true);
  }

  bool getIsUserSeeMessage() {
    return sharedPreferences.getBool(prefsUserWarningMessageKey) ?? false;
  }

  Future<bool> removeUserToken() {
    return sharedPreferences.remove(prefsTokenKey);
  }

  Future<bool> removeUserData() {
    return sharedPreferences.remove(prefsUserKey);
  }

  Future<void> saveUserData(userData) async {
    await sharedPreferences.setString(
        prefsUserKey, jsonEncode(userData.toJson()));
  }

  UserModel get userData {
    return UserModel.fromJson(
        jsonDecode(sharedPreferences.getString(prefsUserKey) ?? "{}"));
  }

  bool isUserLoggedIn() {
    print(getUserToken());
    return getUserToken().isNotEmpty;
  }

  Future<void> logOutPref() async {
    await removeUserToken();
    await removeUserData();
  }
}
