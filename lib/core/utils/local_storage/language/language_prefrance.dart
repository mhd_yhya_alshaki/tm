import 'dart:ui';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:tm/core/utils/extensions/extensions.dart';

import '../../language/language_manager.dart';

class LanguagePreferences {
  late final SharedPreferences sharedPreferences;

  final String prefsLanguageKey = 'language';

  LanguagePreferences(this.sharedPreferences);

  AppLanguage getAppLanguage() {
    String? language = sharedPreferences.getString(prefsLanguageKey);
    if (language != null && language.isNotEmpty) {
      return AppLanguage.values
              .firstWhereOrNull((element) => element.name == language) ??
          AppLanguage.english;
    } else {
      return AppLanguage.english;
    }
  }

  Locale getLocal() {
    AppLanguage currentLanguage = getAppLanguage();
    return (AppLanguage.values
                .firstWhereOrNull((element) => element == currentLanguage) ??
            AppLanguage.english)
        .local();
  }

  Future<void> setAppLanguage({required AppLanguage lang}) async {
    AppLanguage currentLanguage = getAppLanguage();
    if (lang == currentLanguage) {
    } else {
      await sharedPreferences.setString(prefsLanguageKey, lang.name);
    }
  }
}
