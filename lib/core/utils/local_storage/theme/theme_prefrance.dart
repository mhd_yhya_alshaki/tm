import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../core/utils/extensions/extensions.dart';
import '../../../../core/utils/resources/resources.dart';

class ThemePreferences {
  late final SharedPreferences sharedPreferences;

  ThemePreferences(this.sharedPreferences);

  String prefsThemeKey = 'theme';

  setTheme({required ThemeData themeData}) async {
    await sharedPreferences.setString(prefsThemeKey, themeData.getValue());
  }

  ThemeData getTheme(context) {
    final EdgeInsets systemGestureInsets =
        MediaQuery.of(context).systemGestureInsets;

    late PageTransitionsTheme pageTransitionsTheme;
    if (systemGestureInsets.left == 0) {
      pageTransitionsTheme = const PageTransitionsTheme(builders: {
        TargetPlatform.android: CupertinoPageTransitionsBuilder(),
        TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
        TargetPlatform.windows: CupertinoPageTransitionsBuilder(),
      });
    } else {
      pageTransitionsTheme = PageTransitionsTheme(builders: {
        TargetPlatform.android: CustomSlidePageTransitionBuilder(),
        TargetPlatform.iOS: CustomSlidePageTransitionBuilder(),
        TargetPlatform.windows: CustomSlidePageTransitionBuilder(),
      });
    }
    String? theme = sharedPreferences.getString(prefsThemeKey);
    if (theme == ThemeDataType.dark.toString()) {
      return darkTheme;
    } else {
      return lightTheme;
    }
  }
}

class CustomSlidePageTransitionBuilder extends PageTransitionsBuilder {
  @override
  Widget buildTransitions<T>(
    PageRoute<T> route,
    BuildContext context,
    Animation<double> animation,
    Animation<double> secondaryAnimation,
    Widget child,
  ) {
    const begin = Offset(1.0, 0.0); // Slide from right to left
    const end = Offset.zero;
    const curve = Curves.easeInOut;

    var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

    return SlideTransition(
      position: animation.drive(tween),
      child: child,
    );
  }
}
