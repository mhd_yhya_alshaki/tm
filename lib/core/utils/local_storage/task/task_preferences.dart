import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:tm/features/task/entities/task_model/task.dart';

class TaskPreferences {
  late final SharedPreferences sharedPreferences;

  static const String prefsTasksKey = 'tasks';
  static const String prefsDeletedTasksKey = 'deleted_tasks';

  TaskPreferences(this.sharedPreferences);

  Future<void> saveTask(Task task) async {
    List<Task> tasks = await getTasks();
    tasks.removeWhere((existingTask) => existingTask.id == task.id);
    tasks.add(task);
    List<String> taskList =
        tasks.map((task) => jsonEncode(task.toJson())).toList();
    await sharedPreferences.setStringList(prefsTasksKey, taskList);
  }

  Future<List<Task>> getTasks() async {
    List<String> taskList =
        sharedPreferences.getStringList(prefsTasksKey) ?? [];
    return taskList.map((task) => Task.fromJson(jsonDecode(task))).toList();
  }

  Future<Task> getTask(int id) async {
    List<Task> tasks = await getTasks();
    return tasks.firstWhere((task) => task.id == id);
  }

  Future<void> updateTask(Task updatedTask) async {
    List<Task> tasks = await getTasks();
    int index = tasks.indexWhere((task) => task.id == updatedTask.id);
    if (index != -1) {
      tasks[index] = updatedTask;
      List<String> taskList =
          tasks.map((task) => jsonEncode(task.toJson())).toList();
      await sharedPreferences.setStringList(prefsTasksKey, taskList);
    }
  }

  Future<void> deleteTask(int taskId) async {
    List<Task> tasks = await getTasks();
    tasks.removeWhere((task) => task.id == taskId);
    List<String> taskList =
        tasks.map((task) => jsonEncode(task.toJson())).toList();
    await sharedPreferences.setStringList(prefsTasksKey, taskList);

    // Add task ID to deleted tasks list for sync
    List<int> deletedTasks = await getDeletedTasks();
    deletedTasks.add(taskId);
    await sharedPreferences.setStringList(
        prefsDeletedTasksKey, deletedTasks.map((id) => id.toString()).toList());
  }

  Future<List<int>> getDeletedTasks() async {
    List<String> deletedTaskList =
        sharedPreferences.getStringList(prefsDeletedTasksKey) ?? [];
    return deletedTaskList.map((id) => int.parse(id)).toList();
  }

  Future<void> clearDeletedTasks() async {
    await sharedPreferences.remove(prefsDeletedTasksKey);
  }
}
