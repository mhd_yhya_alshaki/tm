part of 'helper.dart';

enum _DialogType { success, error, info }

class _DialogLogger {
  DateTime? lastDialogTime;
  String? lastMessage;
  _DialogType? lastDialogType;
  bool isThereDialogShow;

  _DialogLogger({
    this.lastDialogTime,
    this.lastMessage,
    this.lastDialogType,
    this.isThereDialogShow = false,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is _DialogLogger &&
          runtimeType == other.runtimeType &&
          lastDialogTime == other.lastDialogTime &&
          lastMessage == other.lastMessage &&
          isThereDialogShow == other.isThereDialogShow &&
          lastDialogType == other.lastDialogType;

  @override
  int get hashCode =>
      lastDialogTime.hashCode ^ lastMessage.hashCode ^ lastDialogType.hashCode;

  _DialogLogger copyWith(
      {DateTime? lastDialogTime,
      String? lastMessage,
      _DialogType? lastDialogType,
      bool? isThereDialogShow}) {
    return _DialogLogger(
        lastDialogTime: lastDialogTime ?? this.lastDialogTime,
        lastMessage: lastMessage ?? this.lastMessage,
        lastDialogType: lastDialogType ?? this.lastDialogType,
        isThereDialogShow: isThereDialogShow ?? this.isThereDialogShow);
  }
}

class _DialogHelper {
  static _DialogHelper? _instance;
  _DialogLogger dialogLogger = _DialogLogger();

  _DialogHelper._internal();

  bool canShowDialog(
      {required String message, required _DialogType dialogType}) {
    if (dialogLogger.isThereDialogShow) {
      return false;
    }
    if (dialogLogger.lastDialogType == dialogType) {
      if (dialogLogger.lastMessage == message) {
        if (dialogLogger.lastDialogTime != null) {
          if ((DateTime.now()
                  .difference(dialogLogger.lastDialogTime ?? DateTime.now())
                  .inSeconds) <
              2) {
            return false;
          }
        }
      }
    }
    return true;
  }

  static _DialogHelper get instance {
    _instance ??= _DialogHelper._internal();
    return _instance ?? _DialogHelper._internal();
  }

  void showErrorMessage({
    BuildContext? context,
    required String message,
  }) async {
    if (canShowDialog(message: message, dialogType: _DialogType.error)) {
      dialogLogger = dialogLogger.copyWith(
          lastDialogTime: DateTime.now(),
          lastDialogType: _DialogType.error,
          lastMessage: message);
      toastification.dismissAll();
      toastification.show(
        context:
            context ?? appRouter.configuration.navigatorKey.currentContext!,
        type: ToastificationType.error,
        style: ToastificationStyle.flat,
        title: Text(message),
        alignment: Alignment.topRight,
        autoCloseDuration: const Duration(seconds: 3),
        borderRadius: BorderRadius.circular(12.0),
        boxShadow: lowModeShadow,
        showProgressBar: true,
        dragToClose: true,
        pauseOnHover: false,
      );
    }
  }

  void showSuccessMessage({
    required BuildContext context,
    required String message,
  }) async {
    if (canShowDialog(message: message, dialogType: _DialogType.success)) {
      dialogLogger = dialogLogger.copyWith(
          lastDialogTime: DateTime.now(),
          lastDialogType: _DialogType.success,
          lastMessage: message);
      toastification.show(
        context: context,
        type: ToastificationType.success,
        style: ToastificationStyle.flat,
        title: Text(message),
        alignment: AlignmentDirectional.topEnd,
        autoCloseDuration: const Duration(seconds: 3),
        borderRadius: BorderRadius.circular(12.0),
        boxShadow: lowModeShadow,
        showProgressBar: true,
        dragToClose: true,
        pauseOnHover: false,
      );
    }
  }

  void showInfoMessage({
    BuildContext? context,
    required String message,
  }) async {
    if (canShowDialog(message: message, dialogType: _DialogType.info)) {
      dialogLogger = dialogLogger.copyWith(
          lastDialogTime: DateTime.now(),
          lastDialogType: _DialogType.info,
          lastMessage: message);
      toastification.show(
        context: context ?? rootNavigatorKey.currentContext!,
        type: ToastificationType.info,
        style: ToastificationStyle.flat,
        title: Text(message),
        alignment: AlignmentDirectional.topEnd,
        autoCloseDuration: const Duration(seconds: 3),
        borderRadius: BorderRadius.circular(12.0),
        boxShadow: lowModeShadow,
        showProgressBar: true,
        dragToClose: true,
        pauseOnHover: false,
      );
    }
  }

  void showInfoNotification({
    required BuildContext context,
    required String title,
    required String content,
  }) async {
    if (canShowDialog(message: content, dialogType: _DialogType.info)) {
      dialogLogger = dialogLogger.copyWith(
          lastDialogTime: DateTime.now(),
          lastDialogType: _DialogType.info,
          lastMessage: content);
      toastification.show(
        context: context,
        type: ToastificationType.info,
        style: ToastificationStyle.flat,
        title: Text(title),
        description: Text(content),
        icon: const Icon(Icons.notifications_on_rounded),
        alignment: AlignmentDirectional.topEnd,
        autoCloseDuration: const Duration(seconds: 3),
        borderRadius: BorderRadius.circular(12.0),
        boxShadow: lowModeShadow,
        showProgressBar: true,
        dragToClose: true,
        pauseOnHover: false,
      );
    }
  }

  Future<void> showForceUpdateDialog(String downloadLink) async {
    if (canShowDialog(
        message: LocaleKeys.dialogMessage_updateMessage,
        dialogType: _DialogType.error)) {
      dialogLogger = dialogLogger.copyWith(
          lastDialogTime: DateTime.now(),
          lastDialogType: _DialogType.error,
          isThereDialogShow: true,
          lastMessage: LocaleKeys.dialogMessage_updateMessage);
      await showDialog(
        context: appRouter.routerDelegate.navigatorKey.currentContext!,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return PopScope(
            canPop: false,
            child: AlertDialog(
              content: Text(
                LocaleKeys.dialogMessage_updateMessage.tr(),
                softWrap: true,
                textAlign: TextAlign.center,
              ),
              actionsAlignment: MainAxisAlignment.center,
              actions: <Widget>[
                Column(
                  children: [
                    TextButton(
                      onPressed: () async {
                        // if (await canLaunch(downloadLink)) {
                        Helper.instance.routerHelper
                            .openDownloadLink(downloadLink);
                        // } else {
                        //   throw 'Could not launch $downloadLink';
                        // }
                      },
                      child: Text(LocaleKeys.updateNow.tr()),
                    ),
                  ],
                ),
              ],
            ),
          );
        },
      ).then(
        (value) {
          dialogLogger = dialogLogger.copyWith(
              lastDialogTime: DateTime.now(),
              lastDialogType: _DialogType.error,
              isThereDialogShow: false,
              lastMessage: LocaleKeys.dialogMessage_updateMessage);
        },
      );
    }
  }
}
