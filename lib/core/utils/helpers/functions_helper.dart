part of 'helper.dart';

class _FunctionsHelper {
  String getFirstLetters(String input) {
    List<String> words =
        input.split(' '); // Split the input string into a list of words
    List<String> firstLetters = words
        .map((word) => word.characters.firstOrNull ?? '')
        .toList(); // Extract the first letter of each word
    return firstLetters.join(''); // Join the first letters into a single string
  }

  String getFirstLettersWithSpace(String input) {
    List<String> words =
        input.split(' '); // Split the input string into a list of words
    List<String> firstLetters = words
        .map((word) => word.characters.firstOrNull ?? '')
        .toList(); // Extract the first letter of each word
    return firstLetters
        .join(' '); // Join the first letters into a single string
  }

  String createIdFromUrlAndDateTime(String url) {
    // Generate a unique ID using a combination of URL and current timestamp
    String timestamp = DateTime.now().millisecondsSinceEpoch.toString();
    String id = Random().nextInt(5000).toString() +
        url.hashCode.toRadixString(16) +
        timestamp;
    // Ensure the ID is not too long (e.g., limit it to 16 characters)
    if (id.length > 16) {
      id = id.substring(0, 16);
    }
    return id;
  }

  String extractNumberFromString(String text, int length) {
    RegExp regex = RegExp(r'\b\d{' '$length' r'}\b');
    Match? match = regex.firstMatch(text);
    if (match != null) {
      return match.group(0) ?? '';
    } else {
      // Handle the case where a four-digit number is not found
      return '';
    }
  }

  String showNumber(value) => value == null ? '' : value.toString();
}
