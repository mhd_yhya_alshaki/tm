part of 'helper.dart';

class _DateAndTimeHelper {
  bool isBeforeToday(String date) {
    DateTime? dateTime = DateTime.tryParse(date);
    if (dateTime == null) {
      return false;
    }
    return dateTime.isBefore(DateTime.now());
  }

  String splitDate(String date) {
    List<String> splitDate = date.split(' ');
    if (splitDate.length > 1) {
      splitDate.insert(1, '\n');
    }
    return splitDate.join(' ');
  }

  String formatDate(DateTime? date) =>
      date == null ? '' : DateFormat('yyyy/MM/dd hh:mm a').format(date);

  String formatDuration(Duration duration) {
    String twoDigits(int n) => n.toString().padLeft(2, '0');
    String threeDigits(int n) => n.toString().padLeft(3, '0');

    String hours = twoDigits(duration.inHours);
    String minutes = twoDigits(duration.inMinutes.remainder(60));
    String seconds = twoDigits(duration.inSeconds.remainder(60));
    String milliseconds = threeDigits(duration.inMilliseconds.remainder(1000));

    return '$hours:$minutes:$seconds.$milliseconds';
  }

  String? formatDateJustDate(DateTime? date) =>
      date == null ? null : DateFormat('yyyy/MM/dd').format(date);

  TimeOfDay? timeFromString(String? timeString) {
    try {
      if (timeString != null) {
        final parts = timeString.split(':');
        if (parts.length == 2) {
          final hour = int.parse(parts[0] ?? '');
          final minute = int.parse(parts[1] ?? '');
          if (hour >= 0 && hour < 24 && minute >= 0 && minute < 60) {
            return TimeOfDay(hour: hour, minute: minute);
          }
        }
      }
    } catch (e) {
      // Handle parsing errors if necessary.
    }
    return null; // Return null if parsing fails.
  }

  DateTime? dateFromString(String? date) {
    try {
      if (date != null) {
        return DateTime.tryParse(date);
      }
    } catch (e) {
      // Handle parsing errors if necessary.
    }
    return null; // Return null if parsing fails.
  }

  String formatTime24(TimeOfDay? timeOfDay) => timeOfDay == null
      ? ''
      : DateFormat.Hm().format(DateTime(
          DateTime.now().year,
          DateTime.now().month,
          DateTime.now().day,
          timeOfDay.hour,
          timeOfDay.minute));

  String formatDate24(DateTime? date) =>
      date == null ? '' : DateFormat('yyyy-MM-dd hh:mm:ss').format(date);

  int calculateDaysDifference(DateTime date1, DateTime date2) {
    // Calculate the difference in milliseconds.
    final differenceInMilliseconds =
        date2.millisecondsSinceEpoch - date1.millisecondsSinceEpoch;

    // Convert milliseconds to days and round up.
    final daysDifference =
        (differenceInMilliseconds / (1000 * 60 * 60 * 24)).ceil();

    return daysDifference;
  }

  int countFridaysBetween(DateTime start, DateTime end) {
    if (start.isAfter(end)) {
      return 0; // No Fridays between the dates.
    }

    int fridayCount = 0;
    DateTime currentDate = start;

    while (currentDate.isBefore(end)) {
      if (currentDate.weekday == DateTime.friday) {
        fridayCount++;
      }
      currentDate = currentDate.add(const Duration(days: 1));
    }
    return fridayCount;
  }

  String dateNullSafety({required String? date, String? message}) {
    // remove the spaces from date
    date = date?.trim();
    /*
     check if the date is null or empty:
     if true ->'Pick a Date'
     else-> date
     */
    return (date != null && date.isNotEmpty) ? date : message ?? 'MM/dd/yyyy';
  }

  String convertToDaysAndHours(int hours) {
    if (hours < 0) {
      throw ArgumentError('Hours must be a non-negative integer.');
    }

    final days = hours ~/ 24;
    final remainingHours = hours % 24;

    final daysPart = days > 0 ? '($days days) ' : '';
    final hoursPart = remainingHours > 0 ? '$remainingHours hours' : '';

    return '$daysPart$hoursPart';
  }

  ///return format when its null
  String? customDateFormatAsString(
      {required DateTime? date, DateFormat? format}) {
    String output = dateNullSafety(
        date: date != null
            ? (format ?? Constants.dateFormatText).format(date)
            : null,
        message: format?.pattern);
    return output;
  }
}
