import 'dart:async';

import 'package:easy_localization/easy_localization.dart';

import '../../../generated/locale_keys.g.dart';
import 'helper_imports.dart';

part 'date_helper.dart';
part 'dialog_helper.dart';
part 'functions_helper.dart';
part 'json_formate_helper.dart';
part 'network_helper.dart';
part 'padding_helper.dart';
part 'permission_helper.dart';
part 'router_helper.dart';

class Helper {
  Helper._();

  Helper._internal();

  static final Helper instance = Helper._internal();

  _DateAndTimeHelper get dateHelper => _DateAndTimeHelper();

  _PermissionHandlerHelper get permissionHandlerHelper =>
      _PermissionHandlerHelper();

  _JsonFormatHelper get jsonFormatHelper => _JsonFormatHelper();

  _RouterHelper get routerHelper => _RouterHelper();

  _FunctionsHelper get functionsHelper => _FunctionsHelper();

  _NetworkHelper get networkHelper => _NetworkHelper();

  _PaddingHelper get paddingHelper => _PaddingHelper();

  _DialogHelper get messageHelper => _DialogHelper.instance;
}
