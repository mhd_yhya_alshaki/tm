part of 'helper.dart';

class _RouterHelper {
  GoRoute? getRouteByPath(String path, List<RouteBase> routes) {
    GoRoute? foundRoute;

    void searchRoutes(List<RouteBase> routeList) {
      for (var route in routeList) {
        if (route is GoRoute) {
          if (route.path.replaceAll('/', '') == path.replaceAll('/', '')) {
            foundRoute = route;
            break;
          } else if (route is StatefulShellRoute) {
            if (route.path.replaceAll('/', '') == path.replaceAll('/', '')) {
              foundRoute = route;
              break;
            }
          }
        }
        if (route.routes.isNotEmpty) {
          searchRoutes(route.routes);
        }
      }
    }

    searchRoutes(routes);

    return foundRoute;
  }

  String getFullPathByPath(String path, List<RouteBase> routes) {
    String fullPath = '';
    bool matchFound = false;
    for (var route in routes) {
      if (matchFound) {
        break;
      }
      if (route is GoRoute) {
        if (route.path.replaceAll('/', '') == path.replaceAll('/', '')) {
          fullPath = route.path;
          matchFound = true;
        } else {
          for (var element in route.routes) {
            String nestedPath = getFullPathByPath(path, element.routes);
            if (nestedPath.isNotEmpty) {
              if (element is GoRoute) {
                fullPath = '${route.path}/${(element).path}/$nestedPath';
              } else if (element is StatefulShellRoute) {
                fullPath = '${route.path}/$nestedPath';
              }
              matchFound = true;
              break;
            }
          }
        }
      } else if (route is StatefulShellRoute) {
        for (var element in route.branches) {
          String nestedPath = getFullPathByPath(path, element.routes);
          if (nestedPath.isNotEmpty) {
            fullPath = nestedPath;
            matchFound = true;
            break;
          }
        }
      }
    }
    return fullPath;
  }

  openLink(String? url) async {
    if (url?.isNotEmpty ?? false) {
      Uri uri = Uri.parse(url ?? '');
      // if (await canLaunchUrl(uri)) {
      await launchUrl(
        uri,
      );
      // } else {
      //   throw 'Cant open URL';
      // }
    }
  }

  makeCell(String? phone) async {
    final Uri telUri = Uri(scheme: 'tel', path: phone ?? '0');
    if (await canLaunchUrl(telUri)) {
      await launchUrl(telUri);
    } else {
      throw 'Could not launch $telUri';
    }
  }

  Future<void> openDownloadLink(String? url) async {
    if (url != null && url.isNotEmpty) {
      Uri uri = Uri.parse(url);
      try {
        bool launched =
            await launchUrl(uri, mode: LaunchMode.externalApplication);
        if (!launched) {
          throw 'Could not launch $url';
        }
      } catch (e) {
        print('Error launching URL: $e');
        // Optionally, show a dialog or toast with the error
        // showDialog(
        //   context: context,
        //   builder: (BuildContext context) {
        //     return AlertDialog(
        //       title: Text('Error'),
        //       content: Text('Could not open the link. Please try again later.'),
        //       actions: <Widget>[
        //         TextButton(
        //           onPressed: () {
        //             Navigator.of(context).pop();
        //           },
        //           child: Text('OK'),
        //         ),
        //       ],
        //     );
        //   },
        // );
      }
    } else {
      print('Invalid URL');
      // Optionally, show a dialog or toast with the error
    }
  }

  openRouteAsLinkWithQuery(
      {String? url, Map<String, String>? queryParameters}) async {
    if (url?.isNotEmpty ?? false) {
      Uri uri = Uri.parse('${Constants.baseUrl}/${url ?? ''}');
      uri = uri.replace(queryParameters: queryParameters);
      if (await canLaunchUrl(uri)) {
        await launchUrl(
          uri,
        );
      } else {
        throw 'Cant open URL';
      }
    }
  }

  logout() async {
    AppPreferences appPreferences = instance<AppPreferences>();
    if (appPreferences.userPreferences.getUserToken().isNotEmpty ?? false) {
      // await instance<LogOutUseCase>().execute();
    }
    await appPreferences.userPreferences.logOutPref();
  }

  logoutWithoutApi() async {
    AppPreferences appPreferences = instance<AppPreferences>();
    await appPreferences.userPreferences.logOutPref();
    appRouter.goNamed(RoutesNames.loginRoute);
  }

  clearAndNavigate(String name) {
    while (appRouter.canPop() == true) {
      appRouter.pop();
    }

    appRouter.pushReplacementNamed(name);
  }
}
