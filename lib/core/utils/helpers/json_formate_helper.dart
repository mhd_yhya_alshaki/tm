part of 'helper.dart';

class _JsonFormatHelper {
  bool toBool({required value, bool defaultValue = false}) {
    if (value is int) {
      return value == 1;
    }
    return defaultValue;
  }
}
