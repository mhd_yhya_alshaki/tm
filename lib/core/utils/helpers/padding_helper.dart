part of 'helper.dart';

class _PaddingHelper {
  EdgeInsetsGeometry get smallSymmetricPadding =>
      EdgeInsets.symmetric(vertical: AppSizeH.s3, horizontal: AppSizeW.s3);

  EdgeInsetsGeometry get mediumSymmetricPadding =>
      EdgeInsets.symmetric(vertical: AppSizeH.s9, horizontal: AppSizeW.s9);

  EdgeInsets get bigSymmetricPadding =>
      EdgeInsets.symmetric(vertical: AppSizeH.s18, horizontal: AppSizeW.s18);

  EdgeInsetsGeometry get smallHorizontal =>
      EdgeInsets.symmetric(horizontal: AppSizeW.s3);

  EdgeInsetsGeometry get mediumHorizontal =>
      EdgeInsets.symmetric(horizontal: AppSizeW.s9);

  EdgeInsetsGeometry get bigHorizontal =>
      EdgeInsets.symmetric(horizontal: AppSizeW.s27);

  EdgeInsetsGeometry get smallVertical =>
      EdgeInsets.symmetric(vertical: AppSizeH.s3);

  EdgeInsetsGeometry get mediumVertical =>
      EdgeInsets.symmetric(vertical: AppSizeH.s9);

  EdgeInsetsGeometry get bigVertical =>
      EdgeInsets.symmetric(vertical: AppSizeH.s27);
}
