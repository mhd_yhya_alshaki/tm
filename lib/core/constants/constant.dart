import 'package:easy_localization/easy_localization.dart';

class Constants {
  static const String baseUrl = 'https://dummyjson.com/';
  static const String apiUrl = '$baseUrl/';
  static const String imagePrefix = baseUrl;
  static const int perPage = 25;
  static const String localizationAssetsPath = "assets/lang";
  static DateFormat dateFormatText = DateFormat('dd/MM/yyyy', 'en');
}
