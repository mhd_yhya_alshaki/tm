import 'package:dio/dio.dart';
import 'package:multiple_result/multiple_result.dart';
import 'package:easy_localization/easy_localization.dart';
import '../../../../core/models/failure_model/failure.dart';
import '../../../../core/network/network_info.dart';
import '../../../../generated/locale_keys.g.dart';
import '../../../../core/models/failure_model/failure.dart';
import '{{{failure_path}}}';
import '../../../../core/network/network_info.dart';
import '../../entities/{{module_name}}.dart';
{{#has_parameters}}import '../../entities/{{module_name}}_parameters.dart';{{/has_parameters}}
import '../../domain/repositories/{{module_name}}_repository.dart';
import '../datasources/{{module_name}}_datasource.dart';
class {{module_name.pascalCase()}}RepositoryImplementation implements {{module_name.pascalCase()}}Repository {
  final {{module_name.pascalCase()}}Datasource datasource;

 final NetworkInfo _networkInfo;
  {{module_name.pascalCase()}}RepositoryImplementation(this.datasource,this._networkInfo);

  @override
  Future<Result< {{module_name.pascalCase()}},FailureModel>> call({{#has_parameters}}{{> parameters }} parameters{{/has_parameters}}) async {

      if (await _networkInfo.isConnected) {
      try {
     final response = await datasource.call({{#has_parameters}}parameters{{/has_parameters}});
      if (response.response.statusCode == 200 ||
      response.response.statusCode == 201) {
      return Success(response.data.data);
      } else {
      return Error(FailureModel.fromJson(response.response.data));
      }
      } on DioException catch (e) {
      if (e.type == DioExceptionType.cancel) {
      return Error(
      FailureModel(message: DioExceptionType.cancel.toString()));
      }
      return Error(FailureModel.fromJson(e.response?.data ?? defaultError));
      }
      } else {
      return Error(
      FailureModel(message: LocaleKeys.dioError_noInternetError.tr()));
      }
  }
}
