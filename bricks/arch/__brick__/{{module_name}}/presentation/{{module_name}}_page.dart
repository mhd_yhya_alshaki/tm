import'package:flutter/material.dart';
class {{module_name.pascalCase()}}Page extends StatefulWidget {
  const {{module_name.pascalCase()}}Page({super.key});

  @override
  State<{{module_name.pascalCase()}}Page> createState() => _{{module_name.pascalCase()}}PageState();
}

class _{{module_name.pascalCase()}}PageState extends State<{{module_name.pascalCase()}}Page> {
  @override
  Widget build(BuildContext context) {
    return Scaffold();
  }
}
