import 'package:freezed_annotation/freezed_annotation.dart';
part '{{module_name}}.freezed.dart';
part '{{module_name}}.g.dart';

@freezed
class {{module_name.pascalCase()}} with _${{module_name.pascalCase()}} {
const factory  {{module_name.pascalCase()}}({
@JsonKey(name: 'name') String? name,
}) = _{{module_name.pascalCase()}};

factory  {{module_name.pascalCase()}}.fromJson(Map<String, Object?> json) => _${{module_name.pascalCase()}}FromJson(json);
}

