import 'package:dio/dio.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:multiple_result/multiple_result.dart';
import 'package:retrofit/retrofit.dart';
import 'package:tm/core/models/basic_input/pagination_input/pagination_input.dart';
import 'package:tm/core/models/basic_models/default_success_model/DefaultSuccessModel.dart';
import 'package:tm/core/network/network_info.dart';
import 'package:tm/core/utils/local_storage/task/task_preferences.dart';
import 'package:tm/features/task/data/datasources/task_datasource.dart';
import 'package:tm/features/task/data/repositories/task_repository_implementation.dart';
import 'package:tm/features/task/entities/task_model/task.dart';
import 'package:tm/features/task/entities/todos/todos_response.dart';

import 'task_repository_implementation_test.mocks.dart';

@GenerateMocks([TaskDatasource, NetworkInfo, TaskPreferences])
void main() {
  late MockTaskDatasource mockTaskDatasource;
  late MockNetworkInfo mockNetworkInfo;
  late MockTaskPreferences mockTaskPreferences;
  late TaskRepositoryImplementation repository;

  setUp(() {
    mockTaskDatasource = MockTaskDatasource();
    mockNetworkInfo = MockNetworkInfo();
    mockTaskPreferences = MockTaskPreferences();
    repository = TaskRepositoryImplementation(
      mockTaskDatasource,
      mockNetworkInfo,
      mockTaskPreferences,
    );
  });

  group('fetch', () {
    final pagination = PaginationInput(perPage: 10, page: 1);
    final tTaskList = [
      Task(id: 1, todo: 'Test Todo', completed: false, userId: 1),
    ];

    test('should return tasks when the call to datasource is successful',
        () async {
      // arrange
      when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      when(mockTaskDatasource.fetch(limit: 10, skip: 0)).thenAnswer(
        (_) async => HttpResponse(
          TodosResponseModel(todos: tTaskList),
          Response(
              data: {'todos': tTaskList.map((task) => task.toJson()).toList()},
              statusCode: 200,
              requestOptions: RequestOptions()),
        ),
      );

      // act
      final result = await repository.fetch(pagination);

      // assert
      verify(mockTaskDatasource.fetch(limit: 10, skip: 0));
      expect(result, isA<Success>());
    });

    test('should return error when there is no internet connection', () async {
      // arrange
      when(mockNetworkInfo.isConnected).thenAnswer((_) async => false);

      // act
      final result = await repository.fetch(pagination);

      // assert
      verifyZeroInteractions(mockTaskDatasource);
      expect(result, isA<Error>());
    });
  });

  group('create', () {
    final tTask = Task(id: 1, todo: 'Test Todo', completed: false, userId: 1);

    test('should create task when the call to datasource is successful',
        () async {
      // arrange
      when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      when(mockTaskDatasource.create(
              todo: tTask.todo,
              completed: tTask.completed,
              userId: tTask.userId))
          .thenAnswer(
        (_) async => HttpResponse(
          tTask,
          Response(
              data: tTask.toJson(),
              statusCode: 201,
              requestOptions: RequestOptions()),
        ),
      );

      // act
      final result = await repository.create(
        todo: tTask.todo,
        completed: tTask.completed,
        userId: tTask.userId,
      );

      // assert
      verify(mockTaskDatasource.create(
          todo: tTask.todo, completed: tTask.completed, userId: tTask.userId));
      expect(result, isA<Success>());
    });

    test('should save task locally when there is no internet connection',
        () async {
      // arrange
      when(mockNetworkInfo.isConnected).thenAnswer((_) async => false);

      // act
      final result = await repository.create(
        todo: tTask.todo,
        completed: tTask.completed,
        userId: tTask.userId,
      );

      // assert
      verifyZeroInteractions(mockTaskDatasource);
      verify(mockTaskPreferences.saveTask(any));
      expect(result, isA<Success>());
    });
  });

  group('update', () {
    final tTask = Task(id: 1, todo: 'Updated Todo', completed: true, userId: 1);

    test('should update task when the call to datasource is successful',
        () async {
      // arrange
      when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      when(mockTaskDatasource.update(
              id: tTask.id, todo: tTask.todo, completed: tTask.completed))
          .thenAnswer(
        (_) async => HttpResponse(
          tTask,
          Response(
              data: tTask.toJson(),
              statusCode: 200,
              requestOptions: RequestOptions()),
        ),
      );

      // act
      final result = await repository.update(
        id: tTask.id,
        todo: tTask.todo,
        completed: tTask.completed,
      );

      // assert
      verify(mockTaskDatasource.update(
          id: tTask.id, todo: tTask.todo, completed: tTask.completed));
      expect(result, isA<Success>());
    });

    test('should update task locally when there is no internet connection',
        () async {
      // arrange
      when(mockNetworkInfo.isConnected).thenAnswer((_) async => false);

      // act
      final result = await repository.update(
        id: tTask.id,
        todo: tTask.todo,
        completed: tTask.completed,
      );

      // assert
      verifyZeroInteractions(mockTaskDatasource);
      verify(mockTaskPreferences.updateTask(any));
      expect(result, isA<Success>());
    });
  });

  group('delete', () {
    final tTaskId = 1;

    test('should delete task when the call to datasource is successful',
        () async {
      // arrange
      when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      when(mockTaskDatasource.delete(tTaskId)).thenAnswer(
        (_) async => HttpResponse(
          const DefaultSuccessModel(success: 'success'),
          Response(
              data: const DefaultSuccessModel(success: 'success').toJson(),
              statusCode: 200,
              requestOptions: RequestOptions()),
        ),
      );

      // act
      final result = await repository.delete(tTaskId);

      // assert
      verify(mockTaskDatasource.delete(tTaskId));
      expect(result, isA<Success>());
    });

    test(
        'should mark task for deletion locally when there is no internet connection',
        () async {
      // arrange
      when(mockNetworkInfo.isConnected).thenAnswer((_) async => false);

      // act
      final result = await repository.delete(tTaskId);

      // assert
      verifyZeroInteractions(mockTaskDatasource);
      verify(mockTaskPreferences.deleteTask(tTaskId));
      expect(result, isA<Success>());
    });
  });

  group('show', () {
    final tTaskId = 1;
    final tTask =
        Task(id: tTaskId, todo: 'Test Todo', completed: false, userId: 1);

    test('should return task when the call to datasource is successful',
        () async {
      // arrange
      when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      when(mockTaskDatasource.show(tTaskId)).thenAnswer(
        (_) async => HttpResponse(
          tTask,
          Response(
              data: tTask.toJson(),
              statusCode: 200,
              requestOptions: RequestOptions()),
        ),
      );

      // act
      final result = await repository.show(tTaskId);

      // assert
      verify(mockTaskDatasource.show(tTaskId));
      expect(result, isA<Success>());
    });

    test(
        'should return task from local storage when there is no internet connection',
        () async {
      // arrange
      when(mockNetworkInfo.isConnected).thenAnswer((_) async => false);
      when(mockTaskPreferences.getTask(tTaskId)).thenAnswer((_) async => tTask);

      // act
      final result = await repository.show(tTaskId);

      // assert
      verifyZeroInteractions(mockTaskDatasource);
      verify(mockTaskPreferences.getTask(tTaskId));
      expect(result, isA<Success>());
    });
  });
}
