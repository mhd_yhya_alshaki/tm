import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:tm/features/task/data/repositories/task_repository_implementation.dart';

// Import the generated mocks file
import 'features/task/data/repositories/task_repository_implementation_test.mocks.dart';

void main() {
  late MockTaskDatasource mockTaskDatasource;
  late MockNetworkInfo mockNetworkInfo;
  late MockTaskPreferences mockTaskPreferences;
  late TaskRepositoryImplementation repository;

  setUp(() {
    mockTaskDatasource = MockTaskDatasource();
    mockNetworkInfo = MockNetworkInfo();
    mockTaskPreferences = MockTaskPreferences();
    repository = TaskRepositoryImplementation(
      mockTaskDatasource,
      mockNetworkInfo,
      mockTaskPreferences,
    );

    // Mock behavior for isConnected method
    when(mockNetworkInfo.isConnected)
        .thenAnswer((_) async => true); // Example: Always return true
  });

  testWidgets('Example Widget Test', (WidgetTester tester) async {
    // Build widget and perform tests
  });
}
